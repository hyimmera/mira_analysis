# Insta?ll packages if it's necessary 
#install.packages("dplyr","plyr","stringr","data.table","ggplot2","ggrepel","ggthemes","generics","stringr","plyr","gridExtra","grid","devtools","viridis","hrbrthemes","GLDEX","TSDT","rstudioapi")

#Load needed libraries
library(dplyr)
library(plyr)
library(stringr)
library(data.table)
library(ggplot2)
library(ggrepel)
library(ggthemes)
library(generics)
library(stringr)
library(plyr)
library(readxl)
library(gridExtra)
library(grid)
library(devtools)
library(viridis)
library(hrbrthemes)
library(GLDEX)
library(TSDT)
library(rstudioapi)

# Set working directory
cur_dir = dirname(getSourceEditorContext()$path)
setwd(cur_dir)

#Make a new dir where all plots will be located
dir.create("plots_Descriptive_MIRA")

#Make a new dir where all statistic tests will be located
dir.create("statistics_Descriptive_MIRA")

#Create a folder in which the filtered files of MIRA dataset will be located
dir.create("filtering_V_genes_Descriptive_MIRA")

#Create a folder for supplementary material
dir.create("supplementary_Descriptive_MIRA")

# Change directory to acces MIRA dataset
setwd("../5.Repositories/MIRA_dataset")



#########################################################################################################
###############Processing MIRA dataset (SARS-CoV-2 specific TCRs) #######################################

#Loading file for subject metadata
MIRA_subject_metadata = read.csv(file = "subject-metadata.csv", header = T)

#Load file for minigene dataset

#####################################################################################

MIRA_minigene_detail.csv = read.csv(file = "minigene-detail.csv", header = T)
#make a new collumn in minegene dataframe "THE CDR3 AA seq"
CDR3 = str_split(MIRA_minigene_detail.csv$TCR.BioIdentity, "\\+", simplify = TRUE)[,1]
MIRA_minigene_detail.csv = cbind(MIRA_minigene_detail.csv, CDR3)

######FILTERING  minigene dataset
#Filtering for C/F,W 
MIRA_minigene_detail.csv = MIRA_minigene_detail.csv[which((str_sub(MIRA_minigene_detail.csv$CDR3, 1, 1) == "C") & (str_sub(MIRA_minigene_detail.csv$CDR3, -1, -1) %in% c("F","W"))),]
#Filtering for unproductive rearrangements
MIRA_minigene_detail.csv = MIRA_minigene_detail.csv[which(!grepl("unproductive", MIRA_minigene_detail.csv$CDR3)),]
#Filtering for special characters
MIRA_minigene_detail.csv = MIRA_minigene_detail.csv[which(!grepl("\\X", MIRA_minigene_detail.csv$CDR3) & !grepl("\\#", MIRA_minigene_detail.csv$CDR3) & !grepl("\\*", MIRA_minigene_detail.csv$CDR3) & !grepl("\\.", MIRA_minigene_detail.csv$CDR3)),]

#######################################################################################

#Load file for peptide 1 dataset
####################################################################################

MIRA_peptide_detail_ci.csv = read.csv(file = "peptide-detail-ci.csv", header = T)
#make a new collumn in peptide 1 details for CDR3 AA seq
CDR3 = str_split(MIRA_peptide_detail_ci.csv$TCR.BioIdentity, "\\+", simplify = TRUE)[,1]
MIRA_peptide_detail_ci.csv = cbind(MIRA_peptide_detail_ci.csv, CDR3)

#FILTERING peptide 1 dataframe

#Filtering for C/F,W 
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which((str_sub(MIRA_peptide_detail_ci.csv$CDR3, 1, 1) == "C") & (str_sub(MIRA_peptide_detail_ci.csv$CDR3, -1, -1) %in% c("F","W"))),]
#Filtering for unproductive rearrangements
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(!grepl("unproductive", MIRA_peptide_detail_ci.csv$CDR3)),]
#Filtering for special characters
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(!grepl("\\X", MIRA_peptide_detail_ci.csv$CDR3) & !grepl("\\#", MIRA_peptide_detail_ci.csv$CDR3) & !grepl("\\*", MIRA_peptide_detail_ci.csv$CDR3) & !grepl("\\.", MIRA_peptide_detail_ci.csv$CDR3)),]

###################################################################################


#Load peptide 2 dataset
##############################################################################################

MIRA_peptide_detail_cii.csv = read.csv(file = "peptide-detail-cii.csv", header = T)
#make a new collumn in peptide 1 details for CDR3 AA seq
CDR3 = str_split(MIRA_peptide_detail_cii.csv$TCR.BioIdentity, "\\+", simplify = TRUE)[,1]
MIRA_peptide_detail_cii.csv = cbind(MIRA_peptide_detail_cii.csv, CDR3)

#FILTERING peptide 2 dataset

#Filtering for C/F,W 
MIRA_peptide_detail_cii.csv = MIRA_peptide_detail_cii.csv[which((str_sub(MIRA_peptide_detail_cii.csv$CDR3, 1, 1) == "C") & (str_sub(MIRA_peptide_detail_cii.csv$CDR3, -1, -1) %in% c("F","W"))),]
#Filtering for unproductive rearrangements
MIRA_peptide_detail_cii.csv = MIRA_peptide_detail_cii.csv[which(!grepl("unproductive", MIRA_peptide_detail_cii.csv$CDR3)),]
#Filtering for special characters
MIRA_peptide_detail_cii.csv = MIRA_peptide_detail_cii.csv[which(!grepl("\\X", MIRA_peptide_detail_cii.csv$CDR3) & !grepl("\\#", MIRA_peptide_detail_cii.csv$CDR3) & !grepl("\\*", MIRA_peptide_detail_cii.csv$CDR3) & !grepl("\\.", MIRA_peptide_detail_cii.csv$CDR3)),]

#####################################################################################################


#Rename colnames for Antigen target (ORF) in peptide 1 and 2 datasets
colnames(MIRA_peptide_detail_ci.csv)[4]= "ORF"
colnames(MIRA_peptide_detail_cii.csv)[4]= "ORF"


###Filtering MIRA dataset to use only clonotypes detected in Convalescent and Healthy cohorts

#EXPERIMENTS ASSOCIATED WITH CONVALESCENT AND HEALTHY COHORT
experiments_cov_helth = MIRA_subject_metadata[which(MIRA_subject_metadata$Cohort %in% c("Healthy (No known exposure)", "COVID-19-Convalescent")),1]
#SUBSETING minigene pep1 and pep2 panels for experiments associated with Healthy and Convalescent
MIRA_minigene_detail.csv = MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$Experiment %in% experiments_cov_helth),]
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$Experiment %in% experiments_cov_helth),]
MIRA_peptide_detail_cii.csv = MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$Experiment %in% experiments_cov_helth),]
#Subseting subject metadata to keep only info about healthy and convalescent
MIRA_subject_metadata = MIRA_subject_metadata[which(MIRA_subject_metadata$Cohort %in% c("Healthy (No known exposure)", "COVID-19-Convalescent")),]

####Filter in order to remove sequences that appear other times with family and subfamily FS and other times only in family 

###############################################################################################################

#in Minigene dataset
MIRA_minigene_detail.csv = MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$TCR.BioIdentity %nin% c("CASSPGGGGNQPQHF+TCRBV06-02/06-03+TCRBJ01-05", "CASSPGGGGNQPQHF+TCRBV06-02+TCRBJ01-05",
                                                                                                           "CASSLEGAGITDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGITDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                           "CASSLEGAGVTDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGVTDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                           "CASSPSNEQFF+TCRBV06-02/06-03+TCRBJ02-01", "CASSPSNEQFF+TCRBV06-02+TCRBJ02-01",
                                                                                                           "CASSPANEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSPANEQYF+TCRBV06-02+TCRBJ02-07",
                                                                                                           "CASSQGGYEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSQGGYEQYF+TCRBV06-02+TCRBJ02-07")),]

#in peptide 1 dataset
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$TCR.BioIdentity %nin% c("CASSPGGGGNQPQHF+TCRBV06-02/06-03+TCRBJ01-05", "CASSPGGGGNQPQHF+TCRBV06-02+TCRBJ01-05",
                                                                                                                 "CASSLEGAGITDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGITDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                                 "CASSLEGAGVTDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGVTDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                                 "CASSPSNEQFF+TCRBV06-02/06-03+TCRBJ02-01", "CASSPSNEQFF+TCRBV06-02+TCRBJ02-01",
                                                                                                                 "CASSPANEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSPANEQYF+TCRBV06-02+TCRBJ02-07",
                                                                                                                 "CASSQGGYEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSQGGYEQYF+TCRBV06-02+TCRBJ02-07")),]


#in peptide 2 dataset
MIRA_peptide_detail_cii.csv = MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$TCR.BioIdentity %nin% c("CASSPGGGGNQPQHF+TCRBV06-02/06-03+TCRBJ01-05", "CASSPGGGGNQPQHF+TCRBV06-02+TCRBJ01-05",
                                                                                                                    "CASSLEGAGITDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGITDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                                    "CASSLEGAGVTDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGVTDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                                    "CASSPSNEQFF+TCRBV06-02/06-03+TCRBJ02-01", "CASSPSNEQFF+TCRBV06-02+TCRBJ02-01",
                                                                                                                    "CASSPANEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSPANEQYF+TCRBV06-02+TCRBJ02-07",
                                                                                                                    "CASSQGGYEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSQGGYEQYF+TCRBV06-02+TCRBJ02-07")),]

###########################################################################################################################################################################################################



#Change directory to the folder associated with pseudogene and orf filtering to keep only productive V genes
setwd("../../1.Descriptive_Analysis_MIRA_dataset/filtering_V_genes_Descriptive_MIRA")

###############################################################################################################
###########################Filter 3 MIRA dataframes (minigene, peptide 1, peptide 2) for pseudogenes/ORFs######

#Function to keep only functional V genes and remove ORFs and pseudogenes AND remove V genes reported as "X" / Function variables : X = working dataframe , Y = working column , Z = name of the output file
functional_only = function(X, Y, Z) {
  
  X = X[which(
    !grepl("V01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("V1-", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("03-02", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("03-2", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &    
      !grepl("3-02", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &    
      !grepl("3-2", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &    
      
      !grepl("05-03", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("05-3", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V5-3", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V5-03", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("05-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("05-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V5-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) & 
      !grepl("V5-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("06-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("06-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V6-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V6-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("07-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("07-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("V7-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V7-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("12-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("12-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("12-2", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("12-02", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("V17-", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("V17", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("21-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("21-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) & 
      
      !grepl("23-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("23-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("V26-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V26-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("-X", str_split(X[,Y], "\\+", simplify = TRUE)[,2])),]
  
  X = X[!is.na(X[,Y]),]
  
  
  write.csv(X, Z, row.names = FALSE)
  
}

#Run the function to remove pseudogenes/ORFs and V genes reported as "X" in MIRA datasets 

functional_only(X = MIRA_peptide_detail_ci.csv, Y = 1, Z = "MIRA_peptide_detail_ci_FILTERED.csv")

functional_only(X = MIRA_peptide_detail_cii.csv, Y = 1, Z = "MIRA_peptide_detail_cii_FILTERED.csv")

functional_only(X = MIRA_minigene_detail.csv, Y = 1, Z = "MIRA_minigene_detail_FILTERED.csv")

## now load filtered for pseudogenes/ORFs and "X" V genes in MIRA dataset 

#pep 1
MIRA_peptide_detail_ci.csv = read.csv("MIRA_peptide_detail_ci_FILTERED.csv", header = TRUE)
#pep2
MIRA_peptide_detail_cii.csv = read.csv("MIRA_peptide_detail_cii_FILTERED.csv", header = TRUE)
#minigene
MIRA_minigene_detail.csv = read.csv("MIRA_minigene_detail_FILTERED.csv")


################ END of MIRA dataset processing############################################################################################################
#####################################################################################################################




################ Descriptive analysis approaching Frequency and Expansion of clonotypes in MIRA dataset#################
#######################################################################################################################


#Creating THE_DF, a data frame with all clonotypes, their mean expansion (between experiments) and frequency (in the sample, associated with the subjects)

#Find all unique clonotypes
All_the_clonotypes = unique(c(MIRA_minigene_detail.csv$TCR.BioIdentity, MIRA_peptide_detail_ci.csv$TCR.BioIdentity, MIRA_peptide_detail_cii.csv$TCR.BioIdentity))
#Make a dataframe with them
THE_DF = data.frame(Clonotypes = All_the_clonotypes, Frequency = "", Mean_expansion = "", Source = "")

#run a loop in order to fill THE_DF dataframe with MEAN EXPANSION and FREQUENCY info for every clonotype
for (i in 1:nrow(THE_DF)) {
  #define current clonotype
  current_clonotype = THE_DF$Clonotypes[i]
  #experiments with this specific clonotype
  experiments_cur_clonotype = unique(c(MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$TCR.BioIdentity == current_clonotype),3],
                                       MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$TCR.BioIdentity == current_clonotype),3],
                                       MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$TCR.BioIdentity == current_clonotype),3]))
  
  #vector with expansion values from all experiments in this current clonotype
  expansion_values_cur_clono = as.numeric(vector())
  #loop to find clonal expansion of current clonotype in evry experiment
  for (y in 1:length(experiments_cur_clonotype)) {
    #define current experiment
    current_experiment = experiments_cur_clonotype[y]
    #find all clonotypes in this experiment
    all_clonotypes_cur_exp = c(MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$Experiment == current_experiment),1],
                               MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$Experiment == current_experiment),1],
                               MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$Experiment == current_experiment),1])
    
    #how many clonotypes in this experiment ?
    n_all_clonotypes_cur_exp = length(all_clonotypes_cur_exp)
    # how many times the current clonotype in this experiment
    n_cur_clonotype_cur_experiment = length(all_clonotypes_cur_exp[all_clonotypes_cur_exp == current_clonotype])
    # clonal expansion cur clonotype in current experiment
    clonal_expansion_cur_exp_cur_clono = n_cur_clonotype_cur_experiment/n_all_clonotypes_cur_exp
    expansion_values_cur_clono[y] = clonal_expansion_cur_exp_cur_clono
    
  }
  #mean clonal expansion for cur clono
  mean_expansion_cur_clono = mean(expansion_values_cur_clono)
  THE_DF$Mean_expansion[i] = mean_expansion_cur_clono
  
  #Frequency of current clonotype in sample 
  
  #USE EXPERIMENTS TO FIND THE SUBJECTS FOR current clonotype
  subjects_cur_clonotype = unique(MIRA_subject_metadata[which(MIRA_subject_metadata$Experiment %in% experiments_cur_clonotype),2])
  Frequency_cur_clonotype = (length(subjects_cur_clonotype))/113
  THE_DF$Frequency[i] = Frequency_cur_clonotype
  
  
  #check what repository the current clonotypes of THE_DF comes from
  
  #check pep1
  if(current_clonotype %in% MIRA_peptide_detail_ci.csv$TCR.BioIdentity & current_clonotype %nin% 
     MIRA_peptide_detail_cii.csv$TCR.BioIdentity & current_clonotype %nin% MIRA_minigene_detail.csv$TCR.BioIdentity)
  {THE_DF$Source[i] = "peptide ci"}
  #check pep2
  if(current_clonotype %in% MIRA_peptide_detail_cii.csv$TCR.BioIdentity & current_clonotype %nin% 
     MIRA_peptide_detail_ci.csv$TCR.BioIdentity & current_clonotype %nin% MIRA_minigene_detail.csv$TCR.BioIdentity)
  {THE_DF$Source[i] = "peptide cii"}
  #check minigene
  if(current_clonotype %in% MIRA_minigene_detail.csv$TCR.BioIdentity & current_clonotype %nin% 
     MIRA_peptide_detail_ci.csv$TCR.BioIdentity & current_clonotype %nin% MIRA_peptide_detail_cii.csv$TCR.BioIdentity)
  {THE_DF$Source[i] = "minigene"}
  #check pep1 and pep2
  if(current_clonotype %in% MIRA_peptide_detail_ci.csv$TCR.BioIdentity & current_clonotype %in% 
     MIRA_peptide_detail_cii.csv$TCR.BioIdentity & current_clonotype %nin% MIRA_minigene_detail.csv$TCR.BioIdentity)
  {THE_DF$Source[i] = "peptide ci and cii"}
  #check pep1 and minigene
  if(current_clonotype %in% MIRA_peptide_detail_ci.csv$TCR.BioIdentity & current_clonotype %nin% 
     MIRA_peptide_detail_cii.csv$TCR.BioIdentity & current_clonotype %in% MIRA_minigene_detail.csv$TCR.BioIdentity)
  {THE_DF$Source[i] = "peptide ci and minigene"}
  #check pep2 and minigene
  if(current_clonotype %nin% MIRA_peptide_detail_ci.csv$TCR.BioIdentity & current_clonotype %in% 
     MIRA_peptide_detail_cii.csv$TCR.BioIdentity & current_clonotype %in% MIRA_minigene_detail.csv$TCR.BioIdentity)
  {THE_DF$Source[i] = "peptide cii and minigene"}
  
  
  print(i)
}

### numeric value manipulation
THE_DF$Frequency = as.numeric(THE_DF$Frequency)*100
THE_DF$Mean_expansion = as.numeric(THE_DF$Mean_expansion)*100

#Make a scater plot / to show correlation between clonal expansion and frequency for each clonotype

#change directory to the plots folder
setwd("../plots_Descriptive_MIRA")

pdf("scater plot for Frequency and Mean expansion of MIRA dataset clonotypes.pdf", width = 13)

ggplot(THE_DF, aes(Frequency, Mean_expansion), color = "grey50",
       show.legend = FALSE, alpha = 0.2, size = 1)  +  geom_point() + theme_classic() + xlab("Frequency (% of total subjects)") + ylab("Mean Clonal Expansion")

dev.off()

#change directory to the supplementary folder of Descriptive MIRA
setwd("../supplementary_Descriptive_MIRA")

#Write THE_DF as a data frame with all unique clonotypes of MIRA dataset after filtering
write.csv(THE_DF, "MIRA dataset unique clonotypes after filtering.csv")

######################################### P L O T  1  ##################
# FUNCTION TO take some clonos with top freq

#Make a function that keeps only the number of most frequent clonotypes I want
Clonos = function(x) {
  x_top_freq_clonotypes = head(THE_DF[as.numeric(order(THE_DF$Frequency, decreasing = TRUE)),][,1], x)
  return(x_top_freq_clonotypes)
}


# choose 6 top clonotypes 

Six_top_clonos = Clonos(6)

#Make a data frame to fill, 1st collumn the clonotype, 2nd Cohort, 3rd depth the rows are determined by the experiments for each clonotype
The_df_clono_cohort_depth = data.frame(Clonotype = "", Cohort = "", Depth_Score = "")

#run this loop to fill the df with info about each clonotype, its depth scores (all range no just the mean value)
#and cohort. For each clonotype despite the samples (experiments) there are so many rows
for (i in 1:length(Six_top_clonos)) {
  #Make a vector with the experiments for current clonotype
  experiments_for_cur_clono = unique(c((MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$TCR.BioIdentity == Six_top_clonos[i]),][,3]),
                                       (MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$TCR.BioIdentity == Six_top_clonos[i]),][,3]),
                                       (MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$TCR.BioIdentity == Six_top_clonos[i]),][,3])))
  
  
  #Make a vector with current clonotype duplicated as many times is needed to reach experiments' number
  The_clonotype = rep(Six_top_clonos[i], length(experiments_for_cur_clono))
  #Make a data frame and put the first collumn The_Clonotype...The rows is as many as the experiments
  DF = data.frame(Clonotype = The_clonotype, Cohort = "", Depth_Score = "")
  #now for each experiment find the cohort and clonal depth 
  for (y in 1:length(experiments_for_cur_clono)) {
    #Find cohort for current subject in current clonotype
    The_Cohort = as.character(unique(MIRA_subject_metadata[which(MIRA_subject_metadata$Experiment == experiments_for_cur_clono[y]),5]))
    #Find depth for current experiment in current clonotype
    
    #find all the clonotypes of this specific experiment
    All_clonotypes_of_this_exp =  c((as.character(MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$Experiment == experiments_for_cur_clono[y]),][,1])),
                                    (as.character(MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$Experiment == experiments_for_cur_clono[y]),][,1])),
                                    (as.character(MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$Experiment == experiments_for_cur_clono[y]),][,1])))
    #find number of times this specific exists in this subject
    number_cur_clono_in_cur_experiment = length(All_clonotypes_of_this_exp[All_clonotypes_of_this_exp == Six_top_clonos[i]])
    #find number of total clonotypes in this subject
    number_all_clono_in_cur_experiment = length(All_clonotypes_of_this_exp)
    The_depth = number_cur_clono_in_cur_experiment/number_all_clono_in_cur_experiment
    
    #put Cohort and depth values for specific subject in DF
    DF[y,2] = The_Cohort
    DF[y,3] = The_depth
  }
  
  The_df_clono_cohort_depth = rbind(The_df_clono_cohort_depth, DF)
}

#dataframe manipulation 
The_df_clono_cohort_depth = The_df_clono_cohort_depth[-1,]

The_df_clono_cohort_depth$Depth_Score = as.numeric(The_df_clono_cohort_depth$Depth_Score)

The_df_clono_cohort_depth$Depth_Score = 100 * The_df_clono_cohort_depth$Depth_Score 

#make a new collumn in table The_df_clono_cohort_depth to put Frequency scores
The_df_clono_cohort_depth = cbind(The_df_clono_cohort_depth, Frequency = "")

#add Frequency in table The_df_clono_cohort_depth with the below loop
for (i in 1:nrow(The_df_clono_cohort_depth)) {
  current_clonotype = The_df_clono_cohort_depth$Clonotype[i]
  current_freq = as.numeric(unique(THE_DF[which(THE_DF$Clonotypes == current_clonotype),2]))
  The_df_clono_cohort_depth$Frequency[i] = current_freq
}
The_df_clono_cohort_depth$Frequency = as.numeric(The_df_clono_cohort_depth$Frequency)

#change directory to the plots' folder
setwd("../plots_Descriptive_MIRA")

#BOXPLOT OF MOST COMMON CLONOTYES AND CLONAL DEPTHS DISTRIBUTION

pdf("Boxplot range of Clonal expansion of most common clonotypes.pdf", width = 13)

ggplot(The_df_clono_cohort_depth, aes(x = reorder(Clonotype, Frequency), y = Depth_Score, fill = Cohort)) + geom_boxplot() + theme_classic() + 
  theme(axis.text.x=element_text(angle=10,hjust=0.5)) + ggtitle("Clonotype expansion range through different cohorts") + xlab("Clonotypes") + ylab("Clonal Expansion")

dev.off()



#######################################################################################################################
########################## End of frequency and expansion analysis in MIRA datasets###############################



####################################################################################################################
###### Statistical analysis   Shapiro  ,  Mann Whitney,    Fisher's

## I want to check the normality in expansion values between different cohorts in the most common clonotypes
##Make a function to do Shapiro test and check normality of the values in order to choose between t test and Mann Whitney 
#The function's input is x (clonotype) and y (cohort)

Shapiro = function(x, y) {
  
  clonotype = as.character(x)
  cohort = as.character(y)
  checking_values = as.numeric(The_df_clono_cohort_depth[which(The_df_clono_cohort_depth$Clonotype == clonotype & The_df_clono_cohort_depth$Cohort == cohort),3])
  results = shapiro.test(checking_values)
  return(results$p.value)
}

# Make a data frame with the Shapiro results for the most common clonotypes
Shapiro_results = data.frame(Clonotypes = Six_top_clonos, p_value_H = "", p_value_C = "")


#Estimate Shapiro only for the 2 most common clonotypes because the other 4 have small sample size
for (i in 1:2) {
  
  
  current_clonotype = Shapiro_results$Clonotypes[i]
  Shapiro_results$p_value_H[i] = Shapiro(current_clonotype, "Healthy (No known exposure)")
  Shapiro_results$p_value_C[i] = Shapiro(current_clonotype, "COVID-19-Convalescent")
  
  
}

#change directory to the statistics of Descriptive MIRA analysis
setwd("../statistics_Descriptive_MIRA")

#Write a file with the statistics for Shapiro
write.csv(Shapiro_results, "Results for Shapiro test 2 most common clonotypes.csv")


#make a function to find the pvalue from mann-whitney test in order to check if there is significant difference in expansion values of most commonon clonotypes between different cohorts
Mann_Whitney = function(x) {
  
  the_clono = as.character(x)
  working_df = The_df_clono_cohort_depth[which(The_df_clono_cohort_depth$Clonotype == the_clono),]
  #number of Healthy EXPERIMENTS
  n_H_exps = as.numeric(sum(working_df$Cohort == "Healthy (No known exposure)"))
  n_C_exps = as.numeric(sum(working_df$Cohort == "COVID-19-Convalescent"))
  man = wilcox.test(working_df$Depth_Score ~ working_df$Cohort, mu=0, alt="two.sided", conf.int=T, conf.level=0.95, paired=F, exact=T, correct=T)
  result_vector = c(man$p.value, n_H_exps, n_C_exps)
  return(result_vector)
  
}


#Make a data frame for Mann_Whitney results
Results_Mann_Whitney = data.frame(Clonotypes = Six_top_clonos, p_value = "", number_Healthy = "", number_Convalescent = "")

#Estimate Mann_Whitney for mosto common clonotypes
for (i in 1:2) {
  
  current_clonotype = Results_Mann_Whitney$Clonotypes[i]
  Results_Mann_Whitney$p_value[i] = Mann_Whitney(current_clonotype)[1]
  Results_Mann_Whitney$number_Healthy[i] = Mann_Whitney(current_clonotype)[2]
  Results_Mann_Whitney$number_Convalescent[i] = Mann_Whitney(current_clonotype)[3]
  
}


#write file for Mann Whitney
write.csv(Results_Mann_Whitney, "Results for Mann Whitney most common clonotypes.csv")


# check cohort distribution for the most common clonotypes and check if there is some statistical significance in this
#Make a function to approach p value with chisquare (yates correction) and Fisher exact test in order to check if there is stat significant difference in cohort distribution
# x input is a clonotype  I WORK WITH SUBJECTS HERE (NO EXPERIMENTS) and output is p value from chi / chi yates correction / fisher     AND  number of Healthy SUBJECTS , number of convalescent SUBJECTS
pvalue_function = function(x) {
  
  #Approaching statistical significance test and p value
  
  #Characterise the number of cohorts in MIRA dataset (ALL CLONOTYPES)
  #Make a data frame like MIRA_subject_metadata but without duplocates in subjects
  MIRA = MIRA_subject_metadata %>%
    group_by(Subject) %>%
    filter(row_number() == n())
  #number of healthy in dataset (all clonotypes)
  n_H_all_clonos = sum(MIRA$Cohort == "Healthy (No known exposure)")
  #number of Convalescent in dataset (all clonotypes)
  n_C_all_clonos = sum(MIRA$Cohort == "COVID-19-Convalescent")
  #Characterize the number of cohorts in the purpose of analysing clonotype
  
  #print healthy and convalescent from all data 
  print("healthy ?????")
  print(n_H_all_clonos)
  print("convalescent ?????")
  print(n_C_all_clonos)
  
  #find the experiments of this current clontype
  experiments_of_cur_clono = unique(c(MIRA_minigene_detail.csv[which(MIRA_minigene_detail.csv$TCR.BioIdentity == as.character(x)),3],
                                      MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$TCR.BioIdentity == as.character(x)),3],
                                      MIRA_peptide_detail_cii.csv[which(MIRA_peptide_detail_cii.csv$TCR.BioIdentity == as.character(x)),3]))
  #find the subjects which are associated with these specific experiments
  subjects_of_cur_clono = unique(MIRA_subject_metadata[which(MIRA_subject_metadata$Experiment %in% experiments_of_cur_clono),2])
  #filter MIRA to keep only information for current clonotype
  MIRA_cur_clono_info = MIRA[which(MIRA$Subject %in% subjects_of_cur_clono),]
  #number of healthy with current clonotype
  n_H_cur_clono = sum(MIRA_cur_clono_info$Cohort == "Healthy (No known exposure)")
  # print(n_H_cur_clono)
  n_C_cur_clono = sum(MIRA_cur_clono_info$Cohort == "COVID-19-Convalescent")
  # print(n_C_cur_clono)
  #make a matrix to be the input for chisquare function
  
  df_for_x2 = data.frame(current_clonotype = c(n_C_cur_clono, n_H_cur_clono), all_clonotypes = c(n_C_all_clonos, n_H_all_clonos))
  row.names(df_for_x2)[1] = "Convalescent"
  row.names(df_for_x2)[2] = "Healthy"
  df_for_x2 = as.matrix(df_for_x2)
  fi = fisher.test(df_for_x2, conf.int = T)
  
  vector_info = c(fi$p.value, n_H_cur_clono, n_C_cur_clono)
  return(vector_info)
  
  
}

#Make a stat data frame for most common clonotypes
The_most_frequent_clonos = data.frame(Clonotype = Six_top_clonos, pvalue_fisher = "", Healthy = "", Convalescent = "")


#Use pvalue_function for the clonotypes i want so i can find which are statistically significant for the difference beetween cohorts.

for(i in 1:nrow(The_most_frequent_clonos)){
  
  #define the clonotype we characterise
  loading_clonotype = The_most_frequent_clonos[i,1]
  #find p value
  pvalues = pvalue_function(loading_clonotype)
  #put p values in data frame
  The_most_frequent_clonos[i,2] = pvalues[1]
  The_most_frequent_clonos[i,3] = pvalues[2]
  The_most_frequent_clonos[i,4] = pvalues[3]
  print(i)
}

#write a file with pvalues from Chi / Chi-Yates / Fisher
write.csv(The_most_frequent_clonos, "pvalues for six most common clonotypes.csv")

#change directory to the Descriptive MIRA polots folder
setwd("../plots_Descriptive_MIRA")

#Make data frame plot for the 1st and 4th most common clonotypes which shape significant cohort differentiation
Df = data.frame(Clonotypes = c(rep(Six_top_clonos[1], 2), rep(Six_top_clonos[4], 2)), number_of_subjects = c(15,11,0,14), Cohort = c("H", "C", "H", "C"))
#change directory to the plots' folder 


pdf("bar plot for cohort distribution in 1st and 4th clonotypes.pdf", width = 13)
ggplot(data=Df, aes(x=Clonotypes, y=number_of_subjects, fill=Cohort)) +
  geom_bar(stat="identity", color="black", position=position_dodge(), width = 0.2)+ theme(axis.text.x=element_text(angle=5,hjust=0.5)) +
  scale_fill_manual(values=c('orange','blue'))

dev.off()

##############################################################################################################
##### END of statistical analysis #############################################################################






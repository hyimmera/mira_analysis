import os, sys, math, re

import matplotlib.pyplot as plt

from sklearn import preprocessing, model_selection, metrics, svm
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import random as rn
import statistics
from scipy.stats import mannwhitneyu

from skimage import feature, transform, measure, filters, morphology, exposure
from sklearn.inspection import permutation_importance
from sklearn import manifold, decomposition
import joblib

PROPS = {
    'boxprops':{'edgecolor':'black'},
    'medianprops':{'color':'black'},
    'whiskerprops':{'color':'black'},
    'capprops':{'color':'black'}
}

FLIERPROPS = {
    'markerfacecolor':'black', 
    'markersize':1, 
    'markeredgecolor':'black'
}

def main():

    mira_path = '../5.Repositories/MIRA_dataset'

    filtered_mira_path = '../1.Descriptive_Analysis_MIRA_dataset/filtering_V_genes_Descriptive_MIRA'

    output_dir = './Exploratory_and_ML_MIRA_Analysis'

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    log_fh = open(output_dir + '/analysis.log','w', encoding='utf-8')

    print('Parsing metadata file', file=log_fh)
    print('Parsing metadata file', file=sys.stderr)
    experiment_map = parse_metadata(mira_path, output_dir, log_fh)

    print('Parsing detail files', file=log_fh)
    print('Parsing detail files', file=sys.stderr)
    data_for_ml_df, list_of_orfs = parse_detail_files(filtered_mira_path, output_dir, experiment_map, log_fh)

    print('Running ML analysis', file=log_fh)
    print('Running ML analysis', file=sys.stderr)
    new_list_of_orfs = run_ML(data_for_ml_df, list_of_orfs, True, output_dir, log_fh)

    if len(list(new_list_of_orfs.keys())) > 0:

        output_dir = output_dir + '/after_feature_selection'
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        
        print('Running ML analysis on reduced feature set', file=log_fh)
        print('Running ML analysis on reduced feature set', file=sys.stderr)
        new_list_of_orfs = run_ML(data_for_ml_df, new_list_of_orfs, False, output_dir, log_fh)
    else:
        print('Running ML analysis on reduced feature set FAILED - No important features discovered', file=log_fh)
    
    log_fh.close()


def parse_metadata(mira_path, output_dir, log_fh):

    print('\tParsing file', file=log_fh)
    print('\tParsing file', file=sys.stderr)
    subject_metadata = pd.read_csv(mira_path + '/subject-metadata.csv', sep=',', header=0, encoding='unicode_escape')
    
    experiment_map = dict()
    samples_per_cohort = dict()
    for i in range(len(subject_metadata['Experiment'])):

        experiment = subject_metadata['Experiment'][i]
        gender = subject_metadata['Gender'][i]
        cohort = subject_metadata['Cohort'][i]

        if pd.isna(subject_metadata['Gender'][i]):
            gender = 'NA'

        if cohort not in samples_per_cohort:
            samples_per_cohort[cohort] = dict()

        if gender not in samples_per_cohort[cohort]:
            samples_per_cohort[cohort][gender] = 1
        else:
            samples_per_cohort[cohort][gender] += 1

        experiment_map[experiment] = cohort

    print('\tAnalyzing cohort information', file=log_fh)
    print('\tAnalyzing cohort information', file=sys.stderr)
    data_samples_per_cohort = list()

    for cohort in samples_per_cohort:

        for gender in samples_per_cohort[cohort]:

            data_samples_per_cohort.append([cohort, gender, samples_per_cohort[cohort][gender]])
            print('\t%s - %s - %d' % (cohort, gender, samples_per_cohort[cohort][gender]), file=log_fh)

    data_samples_per_cohort_df = pd.DataFrame(data_samples_per_cohort, columns=['Cohort', 'Gender', 'N_Samples'])

    print('\tGenerating figure - samples_per_cohort', file=log_fh)
    print('\tGenerating figure - samples_per_cohort', file=sys.stderr)

    fig, ax = plt.subplots(figsize=(12,8))
    g = sns.barplot(data=data_samples_per_cohort_df, x='Cohort', y='N_Samples', hue='Gender')
    plt.xlabel('Cohort', fontsize = 10)
    plt.ylabel('Number of samples', fontsize = 10)
    plt.title('Number of samples per MIRA cohort', fontsize = 10)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=8)
    plt.legend(loc='upper right', fontsize = 10, title='Gender', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
    plt.savefig(output_dir + '/samples_per_cohort.pdf', dpi=600, format='pdf')
    plt.close(fig)

    return experiment_map


def parse_detail_files(mira_path, output_dir, experiment_map, log_fh):

    valid_cohort_list = ['COVID-19-Convalescent', 'Healthy (No known exposure)']
    all_orf_list = list()

    orf_per_cohort = dict()
    total_per_exper = dict()
    uniq_tcrs_per_exper = dict()

    duplicate_tcrs = dict()
    unique_tcrs = dict()

    print('\tParsing file - MIRA_peptide_detail_ci_FILTERED.csv', file=log_fh)
    print('\tParsing file - MIRA_peptide_detail_ci_FILTERED.csv', file=sys.stderr)
    peptide_detail_ci = pd.read_csv(mira_path + '/MIRA_peptide_detail_ci_FILTERED.csv', sep=',', header=0)

    for i in range(len(peptide_detail_ci['TCR.BioIdentity'])):

        tcr_id = peptide_detail_ci['TCR.BioIdentity'][i]
        orf = peptide_detail_ci['ORF'][i]
        experiment = peptide_detail_ci['Experiment'][i]
        cohort = experiment_map[experiment]

        tcr_id = tcr_id.strip('"')
        orf = orf.strip('"')
        experiment = experiment.strip('"')
        cohort = cohort.strip('"')

        if tcr_id not in unique_tcrs:
            unique_tcrs[tcr_id] = 1
        else:
            unique_tcrs[tcr_id] += 1

        if '/' in tcr_id:
            # print(tcr_id)
            # sys.exit()

            regex_match = re.search('(.+TCRBV)(.+?)/(.+?)(\+TCRBJ.+)', tcr_id)

            if regex_match:
                # print('Found V. %s | %s - %s - %s - %s' % (tcr_id, regex_match.group(1), regex_match.group(2), regex_match.group(3), regex_match.group(4)))
                # sys.exit()

                tcr_id_one = regex_match.group(1) + regex_match.group(2) + regex_match.group(4)
                tcr_id_two = regex_match.group(1) + regex_match.group(3) + regex_match.group(4)

                # print(tcr_id_one)
                # print(tcr_id_two)
                # sys.exit()

                if tcr_id not in duplicate_tcrs:
                    duplicate_tcrs[tcr_id] = dict()
                
                duplicate_tcrs[tcr_id][tcr_id_one] = 1
                duplicate_tcrs[tcr_id][tcr_id_two] = 1

            else:
                print('ERROR 1: %s' %(tcr_id,))

        # test_string = '"ab"cd"'
        # print(test_string)
        # test_string = test_string.strip('"')
        # print(test_string)

        # print(tcr_id)
        # print(orf)
        # print(experiment)
        # print(cohort)
        # sys.exit()

        # if cohort != 'Healthy (No known exposure)':
        #     cohort = 'COVID-19-Convalescent'

        if cohort not in valid_cohort_list:
            continue

        if cohort not in uniq_tcrs_per_exper:
            uniq_tcrs_per_exper[cohort] = dict()
        
        if experiment not in uniq_tcrs_per_exper[cohort]:
            uniq_tcrs_per_exper[cohort][experiment] = list()
        
        if tcr_id not in uniq_tcrs_per_exper[cohort][experiment]:
            uniq_tcrs_per_exper[cohort][experiment].append(tcr_id)

        if ',' in orf:

            splitted_orf = orf.split(',')

            for s_orf in splitted_orf:

                if s_orf not in all_orf_list:
                    all_orf_list.append(s_orf)
                
                if cohort not in orf_per_cohort:
                    orf_per_cohort[cohort] = dict()
                
                if experiment not in orf_per_cohort[cohort]:
                    orf_per_cohort[cohort][experiment] = dict()

                if s_orf not in orf_per_cohort[cohort][experiment]:
                    orf_per_cohort[cohort][experiment][s_orf] = 1 / len(splitted_orf)
                    # orf_per_cohort[s_orf][cohort][experiment] = 1.
                else:
                    orf_per_cohort[cohort][experiment][s_orf] += 1 / len(splitted_orf)
                    # orf_per_cohort[s_orf][cohort][experiment] += 1.

        else:

            if orf not in all_orf_list:
                all_orf_list.append(orf)

            if cohort not in orf_per_cohort:
                orf_per_cohort[cohort] = dict()
            
            if experiment not in orf_per_cohort[cohort]:
                orf_per_cohort[cohort][experiment] = dict()

            if orf not in orf_per_cohort[cohort][experiment]:
                orf_per_cohort[cohort][experiment][orf] = 1.
            else:
                orf_per_cohort[cohort][experiment][orf] += 1.

        if experiment not in total_per_exper:
            total_per_exper[experiment] = 1.
        else:
            total_per_exper[experiment] += 1.

    print('\tParsing file - MIRA_peptide_detail_cii_FILTERED.csv', file=log_fh)
    print('\tParsing file - MIRA_peptide_detail_cii_FILTERED.csv', file=sys.stderr)
    peptide_detail_cii = pd.read_csv(mira_path + '/MIRA_peptide_detail_cii_FILTERED.csv', sep=',', header=0)

    for i in range(len(peptide_detail_cii['TCR.BioIdentity'])):

        tcr_id = peptide_detail_cii['TCR.BioIdentity'][i]
        orf = peptide_detail_cii['ORF'][i]
        experiment = peptide_detail_cii['Experiment'][i]
        cohort = experiment_map[experiment]

        tcr_id = tcr_id.strip('"')
        orf = orf.strip('"')
        experiment = experiment.strip('"')
        cohort = cohort.strip('"')

        if tcr_id not in unique_tcrs:
            unique_tcrs[tcr_id] = 1
        else:
            unique_tcrs[tcr_id] += 1

        if '/' in tcr_id:
            # print(tcr_id)
            # sys.exit()

            regex_match = re.search('(.+TCRBV)(.+?)/(.+?)(\+TCRBJ.+)', tcr_id)

            if regex_match:
                # print('Found V. %s | %s - %s - %s - %s' % (tcr_id, regex_match.group(1), regex_match.group(2), regex_match.group(3), regex_match.group(4)))
                # sys.exit()

                tcr_id_one = regex_match.group(1) + regex_match.group(2) + regex_match.group(4)
                tcr_id_two = regex_match.group(1) + regex_match.group(3) + regex_match.group(4)

                # print(tcr_id_one)
                # print(tcr_id_two)
                # sys.exit()

                if tcr_id not in duplicate_tcrs:
                    duplicate_tcrs[tcr_id] = dict()
                
                duplicate_tcrs[tcr_id][tcr_id_one] = 1
                duplicate_tcrs[tcr_id][tcr_id_two] = 1

            else:
                print('ERROR 1: %s' %(tcr_id,))

        # print(tcr_id)
        # print(orf)
        # print(experiment)
        # print(cohort)
        # sys.exit()

        if cohort not in valid_cohort_list:
            continue

        if cohort not in uniq_tcrs_per_exper:
            uniq_tcrs_per_exper[cohort] = dict()
        
        if experiment not in uniq_tcrs_per_exper[cohort]:
            uniq_tcrs_per_exper[cohort][experiment] = list()
        
        if tcr_id not in uniq_tcrs_per_exper[cohort][experiment]:
            uniq_tcrs_per_exper[cohort][experiment].append(tcr_id)

        if ',' in orf:

            splitted_orf = orf.split(',')

            for s_orf in splitted_orf:

                if s_orf not in all_orf_list:
                    all_orf_list.append(s_orf)
                
                if cohort not in orf_per_cohort:
                    orf_per_cohort[cohort] = dict()
                
                if experiment not in orf_per_cohort[cohort]:
                    orf_per_cohort[cohort][experiment] = dict()

                if s_orf not in orf_per_cohort[cohort][experiment]:
                    orf_per_cohort[cohort][experiment][s_orf] = 1 / len(splitted_orf)
                    # orf_per_cohort[s_orf][cohort][experiment] = 1.
                else:
                    orf_per_cohort[cohort][experiment][s_orf] += 1 / len(splitted_orf)
                    # orf_per_cohort[s_orf][cohort][experiment] += 1.

        else:

            if orf not in all_orf_list:
                all_orf_list.append(orf)

            if cohort not in orf_per_cohort:
                orf_per_cohort[cohort] = dict()
            
            if experiment not in orf_per_cohort[cohort]:
                orf_per_cohort[cohort][experiment] = dict()

            if orf not in orf_per_cohort[cohort][experiment]:
                orf_per_cohort[cohort][experiment][orf] = 1.
            else:
                orf_per_cohort[cohort][experiment][orf] += 1.

        if experiment not in total_per_exper:
            total_per_exper[experiment] = 1.
        else:
            total_per_exper[experiment] += 1.
    
    print('\tParsing file - MIRA_minigene_detail_FILTERED.csv', file=log_fh)
    print('\tParsing file - MIRA_minigene_detail_FILTERED.csv', file=sys.stderr)
    minigene_detail = pd.read_csv(mira_path + '/MIRA_minigene_detail_FILTERED.csv', sep=',', header=0)

    for i in range(len(minigene_detail['TCR.BioIdentity'])):

        tcr_id = minigene_detail['TCR.BioIdentity'][i]
        orf = minigene_detail['ORF'][i]
        experiment = minigene_detail['Experiment'][i]
        cohort = experiment_map[experiment]

        tcr_id = tcr_id.strip('"')
        orf = orf.strip('"')
        experiment = experiment.strip('"')
        cohort = cohort.strip('"')

        if tcr_id not in unique_tcrs:
            unique_tcrs[tcr_id] = 1
        else:
            unique_tcrs[tcr_id] += 1

        if '/' in tcr_id:
            # print(tcr_id)
            # sys.exit()

            regex_match = re.search('(.+TCRBV)(.+?)/(.+?)(\+TCRBJ.+)', tcr_id)

            if regex_match:
                # print('Found V. %s | %s - %s - %s - %s' % (tcr_id, regex_match.group(1), regex_match.group(2), regex_match.group(3), regex_match.group(4)))
                # sys.exit()

                tcr_id_one = regex_match.group(1) + regex_match.group(2) + regex_match.group(4)
                tcr_id_two = regex_match.group(1) + regex_match.group(3) + regex_match.group(4)

                # print(tcr_id_one)
                # print(tcr_id_two)
                # sys.exit()

                if tcr_id not in duplicate_tcrs:
                    duplicate_tcrs[tcr_id] = dict()
                
                duplicate_tcrs[tcr_id][tcr_id_one] = 1
                duplicate_tcrs[tcr_id][tcr_id_two] = 1

            else:
                print('ERROR 1: %s' %(tcr_id,))

        # test_string = '"ab"cd"'
        # print(test_string)
        # test_string.strip('"')
        # print(test_string)

        # print(tcr_id)
        # print(orf)
        # print(experiment)
        # print(cohort)
        # sys.exit()

        if cohort not in valid_cohort_list:
            continue

        if cohort not in uniq_tcrs_per_exper:
            uniq_tcrs_per_exper[cohort] = dict()
        
        if experiment not in uniq_tcrs_per_exper[cohort]:
            uniq_tcrs_per_exper[cohort][experiment] = list()
        
        if tcr_id not in uniq_tcrs_per_exper[cohort][experiment]:
            uniq_tcrs_per_exper[cohort][experiment].append(tcr_id)

        if ',' in orf:

            splitted_orf = orf.split(',')

            for s_orf in splitted_orf:

                if s_orf not in all_orf_list:
                    all_orf_list.append(s_orf)
                
                if cohort not in orf_per_cohort:
                    orf_per_cohort[cohort] = dict()
                
                if experiment not in orf_per_cohort[cohort]:
                    orf_per_cohort[cohort][experiment] = dict()

                if s_orf not in orf_per_cohort[cohort][experiment]:
                    orf_per_cohort[cohort][experiment][s_orf] = 1 / len(splitted_orf)
                    # orf_per_cohort[s_orf][cohort][experiment] = 1.
                else:
                    orf_per_cohort[cohort][experiment][s_orf] += 1 / len(splitted_orf)
                    # orf_per_cohort[s_orf][cohort][experiment] += 1.

        else:

            if orf not in all_orf_list:
                all_orf_list.append(orf)

            if cohort not in orf_per_cohort:
                orf_per_cohort[cohort] = dict()
            
            if experiment not in orf_per_cohort[cohort]:
                orf_per_cohort[cohort][experiment] = dict()

            if orf not in orf_per_cohort[cohort][experiment]:
                orf_per_cohort[cohort][experiment][orf] = 1.
            else:
                orf_per_cohort[cohort][experiment][orf] += 1.

        if experiment not in total_per_exper:
            total_per_exper[experiment] = 1.
        else:
            total_per_exper[experiment] += 1.

    for tcr_id in duplicate_tcrs:

        for child_tcr in duplicate_tcrs[tcr_id]:
            
            if child_tcr in unique_tcrs:
                print('%s - %s: %d' % (tcr_id, child_tcr, unique_tcrs[child_tcr],), file=log_fh)
    # sys.exit()

    print('\tReformatting data', file=log_fh)
    print('\tReformatting data', file=sys.stderr)
    data_norm = list()
    for_ml = dict()

    for cohort in orf_per_cohort:

        for experiment in orf_per_cohort[cohort]:

            for orf in all_orf_list:

                tmp_value = 0.

                try:
                    tmp_value = orf_per_cohort[cohort][experiment][orf]
                except KeyError:
                    pass

                data_norm.append([orf, cohort, experiment, 1000 * tmp_value / total_per_exper[experiment]])

                if experiment not in for_ml:
                    for_ml[experiment] = dict()
                
                for_ml[experiment][orf] = tmp_value / total_per_exper[experiment]
                for_ml[experiment]['cohort'] = cohort

    print('\tGenerating figure - tcrs_per_antigen', file=log_fh)
    print('\tGenerating figure - tcrs_per_antigen', file=sys.stderr)
    data_norm_df = pd.DataFrame(data_norm, columns = ['Antigen', 'Cohort', 'Experiment', 'Hits_N'])

    fig, ax = plt.subplots(figsize=(12,8))
    g = sns.boxplot(data=data_norm_df, x='Antigen', y='Hits_N', hue='Cohort', linewidth=0.5, flierprops=FLIERPROPS, **PROPS)
    plt.xlabel('SARS-CoV-2 antigen', fontsize = 10)
    plt.ylabel('Number of TCRs per 1,000 TCRs', fontsize = 10)
    plt.xticks(rotation=20, fontsize=8, ha='right')
    plt.yticks(fontsize=8)
    plt.title('Normalized number of TCRs per SARS-CoV-2 antigen per MIRA cohort', fontsize = 10)
    plt.legend(loc='best', fontsize = 10, title='Cohort', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
    plt.savefig(output_dir + '/tcrs_per_antigen.pdf', dpi=600, format='pdf')
    plt.close(fig)

    for unique_antigen_name in list(data_norm_df.Antigen.unique()):

        # print(unique_antigen_name)

        data_norm_df_antigen = data_norm_df.loc[data_norm_df['Antigen'] == unique_antigen_name]

        data_norm_df_antigen_x = data_norm_df_antigen.loc[data_norm_df_antigen['Cohort'] == valid_cohort_list[0]]
        data_norm_df_antigen_y = data_norm_df_antigen.loc[data_norm_df_antigen['Cohort'] == valid_cohort_list[1]]

        stat_test = mannwhitneyu(data_norm_df_antigen_x['Hits_N'].tolist(), data_norm_df_antigen_y['Hits_N'].tolist(), use_continuity=True, alternative='two-sided')
        print('\t\tMann-WhitneyU %s: samples in healthy %d - samples in convalescent %d - pval %f' % (unique_antigen_name, len(data_norm_df_antigen_y.index), len(data_norm_df_antigen_x.index), stat_test.pvalue), file=log_fh)

    print('\tCalculating normalized number of unique TCRs per sample', file=log_fh)
    print('\tCalculating normalized number of unique TCRs per sample', file=sys.stderr)
    data_uniq_tcrs = list()

    for cohort in uniq_tcrs_per_exper:

        for experiment in uniq_tcrs_per_exper[cohort]:

            data_uniq_tcrs.append([cohort, experiment, 1000 * len(uniq_tcrs_per_exper[cohort][experiment]) / total_per_exper[experiment]])

    print('\tGenerating figure - unique_tcrs_per_cohort', file=log_fh)
    print('\tGenerating figure - unique_tcrs_per_cohort', file=sys.stderr)
    data_uniq_tcrs_df = pd.DataFrame(data_uniq_tcrs, columns=['Cohort', 'Experiment', 'TCRs_Uniq'])

    fig, ax = plt.subplots(figsize=(12,8))
    g = sns.boxplot(data=data_uniq_tcrs_df, x='Cohort', y='TCRs_Uniq', linewidth=0.5, flierprops=FLIERPROPS, **PROPS)
    plt.xlabel('Cohort', fontsize = 10)
    plt.ylabel('Percentage of unique TCRs', fontsize = 10)
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=8)
    plt.title('Normalized number of unique TCRs per experiment per MIRA cohort', fontsize = 10)
    plt.savefig(output_dir + '/unique_tcrs_per_cohort.pdf', dpi=600, format='pdf')
    plt.close(fig)

    data_uniq_tcrs_df_x = data_uniq_tcrs_df.loc[data_uniq_tcrs_df['Cohort'] == valid_cohort_list[0]]
    data_uniq_tcrs_df_y = data_uniq_tcrs_df.loc[data_uniq_tcrs_df['Cohort'] == valid_cohort_list[1]]
    
    stat_test = mannwhitneyu(data_uniq_tcrs_df_x['TCRs_Uniq'].tolist(), data_uniq_tcrs_df_y['TCRs_Uniq'].tolist(), use_continuity=True, alternative='two-sided')
    print('\t\tMann-WhitneyU: %f' % (stat_test.pvalue), file=log_fh)
    print('\t\tMann-WhitneyU: %f' % (stat_test.pvalue), file=sys.stderr)

    print('\tReformatting data for ML', file=log_fh)
    print('\tReformatting data for ML', file=sys.stderr)
    # list_of_orfs = list(orf_per_cohort.keys())

    # header_for_df = ['experiment']
    header_for_df = list()
    for orf in all_orf_list:
        header_for_df.append(orf)
    header_for_df.append('cohort')
    header_for_df.append('label')
    header_for_df.append('color')

    label_map = {
        'COVID-19-Convalescent': 1., 
        'Healthy (No known exposure)': 0.
    }

    color_map = {
        'COVID-19-Convalescent': 'ff7f0e', 
        'Healthy (No known exposure)': '1f77b4'
    }

    print('\tNumber of experiments in ML structure: %d' % (len(for_ml.keys()),), file=log_fh)
    print('\tNumber of experiments in ML structure: %d' % (len(for_ml.keys()),), file=sys.stderr)

    data_for_ml = list()

    for experiment in for_ml:

        tmp_list = list()
        # tmp_list.append(experiment)

        for orf in all_orf_list:
            if orf in for_ml[experiment]:
                tmp_list.append(for_ml[experiment][orf])
            else:
                tmp_list.append(0.)
        
        cohort = for_ml[experiment]['cohort']
        tmp_list.append(cohort)
        tmp_list.append(label_map[cohort])
        tmp_list.append(color_map[cohort])

        data_for_ml.append(tmp_list)

    data_for_ml_df = pd.DataFrame(data_for_ml, columns=header_for_df, dtype='float32')

    return data_for_ml_df, all_orf_list


def run_ML(data_for_ml_df, input_list_of_orfs, is_list, output_dir, log_fh):

    algo_list = ['GaussianNB', 'DT', 'KNN', 'RF', 'SVM']
    # algo_list = ['RF', 'SVM']

    # Scoring scheme
    sc_scheme = ['balanced_accuracy', 'precision', 'recall', 'roc_auc']

    # Cross-validation scheme
    rskf = model_selection.RepeatedStratifiedKFold(n_splits=10, n_repeats=10, random_state=0)

    test_perm = list()
    feat_imp_perm = list()
    feat_imp_perm_dict = dict()
    metric_perm = list()

    for algo in algo_list:

        if is_list == False and algo not in input_list_of_orfs:
            continue

        print('\tOn %s' % (algo,), file=log_fh)
        print('\tOn %s' % (algo,), file=sys.stderr)

        algo_dir = output_dir + '/' + algo

        if not os.path.exists(algo_dir):
            os.mkdir(algo_dir)

        list_of_orfs = list()
        if is_list == True:
            list_of_orfs = input_list_of_orfs
        else:
            list_of_orfs = input_list_of_orfs[algo]
        
        # print('\tRunning TSNE')
        # tsne = manifold.TSNE(n_components=2, init='random', random_state=0, perplexity=200, n_iter=300)
        # Y = tsne.fit_transform(data_for_ml_df[list_of_orfs])

        print('\t\tRunning PCA', file=log_fh)
        print('\t\tRunning PCA', file=sys.stderr)
        pca = decomposition.PCA(n_components=2)
        Y = pca.fit_transform(data_for_ml_df[list_of_orfs])

        data_for_ml_df['dim_1'] = Y[:,0]
        data_for_ml_df['dim_2'] = Y[:,1]

        print('\t\t\tExplained variability - PCA 1: %f - PCA 2: %f' % (pca.explained_variance_ratio_[0], pca.explained_variance_ratio_[1]), file=log_fh)
        print('\t\t\tExplained variability - PCA 1: %f - PCA 2: %f' % (pca.explained_variance_ratio_[0], pca.explained_variance_ratio_[1]), file=sys.stderr)

        print('\t\tGenerating figure - tcrs_per_antigen_pca', file=log_fh)
        print('\t\tGenerating figure - tcrs_per_antigen_pca', file=sys.stderr)

        fig, ax = plt.subplots(figsize=(12,8))
        g = sns.scatterplot(data=data_for_ml_df, x=data_for_ml_df['dim_1'], y=data_for_ml_df['dim_2'], hue='cohort', sizes=(5, 5))
        plt.xlabel('dim_1', fontsize = 10)
        plt.ylabel('dim_2', fontsize = 10)
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=8)
        plt.title('PCA analysis on MIRA dataset', fontsize = 10)
        plt.legend(loc='best', fontsize = 10, title='Cohort', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
        plt.savefig(algo_dir + '/tcrs_per_antigen_pca.pdf', dpi=600, format='pdf')
        plt.close(fig)

        loadings = pca.components_.T * np.sqrt(pca.explained_variance_) # feature correlation with PCs
        # loadings_df = pd.DataFrame(loadings, columns=['PC1', 'PC2', 'PC3', 'PC4', 'PC5', 'PC6', 'PC7', 'PC8', 'PC9', 'PC10', 'PC11'])
        loadings_df = pd.DataFrame(loadings, columns=['PC1', 'PC2'])
        loadings_df['Feature'] = list_of_orfs
        loadings_df.to_csv(algo_dir + '/tcrs_per_antigen_pca_loadings.csv', sep='\t', float_format='%.5f', header=True, index=False)
        # print(loadings_df)

        fig, ax = plt.subplots(figsize=(12,8))
        g = sns.scatterplot(data=loadings_df, x=loadings_df['PC1'], y=loadings_df['PC2'], sizes=(5, 5))
        plt.axhline(y=0, color='gray', linestyle='--', linewidth=1)
        plt.axvline(x=0, color='gray', linestyle='--', linewidth=1)
        plt.xlim((-0.3, 0.3))
        plt.ylim((-0.3, 0.3))
        for index, row in loadings_df.iterrows():
            plt.text(row['PC1'] + 0.003, row['PC2'] + 0.003, row['Feature'], fontdict=dict(color='gray', alpha=0.7, size=8))
        plt.xlabel('PC1', fontsize = 10)
        plt.ylabel('PC2', fontsize = 10)
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=8)
        plt.title('PCA factor loadings', fontsize = 10)
        # plt.legend(loc='best', fontsize = 10, title='Cohort', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
        plt.savefig(algo_dir + '/tcrs_per_antigen_pca_loadings.pdf', dpi=600, format='pdf')
        plt.close(fig)

        # sys.exit()

        # Set input data
        X = data_for_ml_df[list_of_orfs]
        Y = data_for_ml_df['label']
        # print(X)
        # print(Y)
        # sys.exit()

        if algo not in feat_imp_perm_dict:
            feat_imp_perm_dict[algo] = dict()

        classifier = None
        param_grid = None

        if algo == 'RF':
            classifier = RandomForestClassifier(random_state=0)
            param_grid = {
                'bootstrap': [True],
                'max_depth': [10, 30, 90],
                'max_features': [None],
                'n_estimators': [10, 50, 100]
                }
        elif algo == 'SVM':
            classifier = svm.SVC(random_state=0, probability=True)
            param_grid = {
                'kernel': ['rbf'],
                'C': [0.1, 1, 10, 100],
                'gamma': [1, 0.1, 0.01, 0.001]
                }
        elif algo == 'SGB':
            classifier = GradientBoostingClassifier(validation_fraction=0.2, n_iter_no_change=10, random_state=0)
            param_grid = {
                'learning_rate': [0.1, 0.01, 0.001],
                'max_depth': [10, 30, 90],
                'max_features': [None],
                'n_estimators': [10, 50, 100]
                }
        elif algo == 'MLP':
            classifier = MLPClassifier(random_state=0, max_iter=500, solver='sgd')
            param_grid = {
                'hidden_layer_sizes': [(10, 20, 10), (20, 40, 20), (60, 120, 60)],
                'learning_rate': ['adaptive'],
                'batch_size': [10, 30, 50],
                'learning_rate_init': [0.001],
                'validation_fraction': [0.1, 0.2, 0.3]
                }
        elif algo == 'LR':
            classifier = LogisticRegression(random_state=0, max_iter=200)
            param_grid = {
                'penalty': ['l1', 'l2', 'elasticnet'],
                'solver': ['lbfgs', 'saga', 'newton-cg']
                }
        elif algo == 'KNN':
            classifier = KNeighborsClassifier()
            param_grid = {
                'n_neighbors': [2, 5, 10],
                'algorithm': ['ball_tree', 'kd_tree', 'brute'],
                'p': [1, 2]
                }
        elif algo == 'GaussianNB':
            classifier = GaussianNB()
            param_grid = {
                'var_smoothing': [1e-9, 1e-8, 1e-7]
                }
        elif algo == 'DT':
            classifier = DecisionTreeClassifier(random_state = 0)
            param_grid = {
                'max_depth': [10, 30, 90],
                'max_features': [None]
                }
        elif algo == 'AdaBoost':
            classifier = AdaBoostClassifier(random_state = 0)
            param_grid = {
                'n_estimators': [50, 100, 150],
                'learning_rate': [2.0, 1.0, 0.1, 0.01]
                }
        else:
            print('ERROR: unkown ML algorithm ' + algo, file=log_fh)
            print('ERROR: unkown ML algorithm ' + algo, file=sys.stderr)
            sys.exit()

        search = model_selection.GridSearchCV(estimator=classifier, param_grid=param_grid, scoring=sc_scheme, cv=rskf, refit='balanced_accuracy', n_jobs=1, verbose=1)

        for i in range(20):

            print('\t\tSplit %d' % (i,), file=log_fh)
            print('\t\tSplit %d' % (i,), file=sys.stderr)

            X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, test_size=0.3, stratify=Y, random_state=i)

            print('\t\t\tTraining the model', file=log_fh)
            print('\t\t\tTraining the model', file=sys.stderr)
            search.fit(X_train, Y_train)

            print('\t\t\tPrinting training stats in file', file=log_fh)
            print('\t\t\tPrinting training stats in file', file=sys.stderr)
            results_df = pd.DataFrame(search.cv_results_)
            # print(results_df)
            results_df = results_df.sort_values(by=['rank_test_balanced_accuracy'])
            results_df.to_csv(algo_dir + '/grid_search_results.rep' + str(i) + '.csv', sep='\t', float_format='%.3f', header=True, columns=['params', 'rank_test_balanced_accuracy', 'mean_test_balanced_accuracy', 'std_test_balanced_accuracy', 'mean_test_precision', 'std_test_precision', 'mean_test_recall', 'std_test_recall', 'mean_test_roc_auc', 'std_test_roc_auc'])

            joblib.dump(search.best_estimator_, algo_dir + '/saved_model.rep' + str(i) + '.pkl')

            print('\t\t\tApplying best model on test set', file=log_fh)
            print('\t\t\tApplying best model on test set', file=sys.stderr)
            test_prediction = search.best_estimator_.predict(X_test)

            Y_test = list(Y_test)
            cm = metrics.confusion_matrix(Y_test, test_prediction)
            # tn, fp, fn, tp = cm.ravel()
            # tn = float(tn)
            # fp = float(fp)
            # fn = float(fn)
            # tp = float(tp)
            # specificity = 0
            # npv = 0
            # if tn != 0:
            #     specificity = tn / (tn + fp)
            #     npv = tn / (tn + fn)

            grid_fh = open(algo_dir + '/grid_search_results.rep' + str(i) + '.csv','a', encoding='utf-8')
            print('Testing - Accuracy: %0.3f | Precision: %0.3f | Recall: %0.3f | F1: %0.3f | ROC_AUC: %0.3f' % (metrics.balanced_accuracy_score(Y_test, test_prediction), metrics.precision_score(Y_test, test_prediction), metrics.recall_score(Y_test, test_prediction), metrics.f1_score(Y_test, test_prediction), metrics.roc_auc_score(Y_test, test_prediction)), file=grid_fh)
            grid_fh.close()

            print('\t\t\tGenerating figure - test_confusion_matrix.rep' + str(i) + '.pdf', file=log_fh)
            print('\t\t\tGenerating figure - test_confusion_matrix.rep' + str(i) + '.pdf', file=sys.stderr)
            fig = plt.figure(figsize=(8,8))
            plt.title('Confusion matrix')
            sns.heatmap(cm, annot=True)
            plt.savefig(algo_dir + '/test_confusion_matrix.rep' + str(i) + '.pdf', dpi=300, format='pdf')
            plt.close(fig)

            print('\t\t\tRunning feature importance', file=log_fh)
            print('\t\t\tRunning feature importance', file=sys.stderr)
            perm = permutation_importance(search.best_estimator_, X_test, Y_test, scoring='balanced_accuracy', n_repeats=50, n_jobs=1, random_state=i)

            tmp_fh = open(algo_dir + '/permutation_feature_importance.f1.rep' + str(i) + '.csv','w', encoding='utf-8')
            print('Feature\tMean\tSTD', file=tmp_fh)
            for y in perm.importances_mean.argsort():
                print('%s\t%0.3f\t%0.3f' % (list_of_orfs[y], perm.importances_mean[y], perm.importances_std[y]), file=tmp_fh)
                feat_imp_perm.append([algo, list_of_orfs[y], perm.importances_mean[y], 'rep' + str(i)])

                if list_of_orfs[y] not in feat_imp_perm_dict[algo]:
                    feat_imp_perm_dict[algo][list_of_orfs[y]] = list()
                
                feat_imp_perm_dict[algo][list_of_orfs[y]].append(perm.importances_mean[y])
            tmp_fh.close()

            print('\t\t\tCalculating curve statistics', file=log_fh)
            print('\t\t\tCalculating curve statistics', file=sys.stderr)
            test_prediction = search.best_estimator_.predict_proba(X_test)

            for cutoff_int in range(10, 100, 2):

                cutoff = cutoff_int / 100

                temp_tp = 0
                temp_fp = 0
                temp_tn = 0
                temp_fn = 0

                for p_index in range(len(test_prediction)):

                    # print(test_prediction)
                    # print(Y_test)
                    # sys.exit()

                    pred_score = test_prediction[p_index][1]
                    true_label = Y_test[p_index]

                    # print(pred_score)
                    # print(true_score)
                    # sys.exit()

                    pred_label = 0.
                    if pred_score >= cutoff:
                        pred_label = 1.
                    
                    if true_label == 1.:
                        if pred_label == true_label:
                            temp_tp += 1
                        else:
                            temp_fn += 1
                    else:
                        if pred_label == true_label:
                            temp_tn += 1
                        else:
                            temp_fp += 1
                
                temp_precision = 0
                temp_sensitivity = 0
                temp_specificity = 0
                temp_npv = 0
                temp_f1 = 0

                if temp_tp != 0:
                    temp_precision = temp_tp / (temp_tp + temp_fp)
                    temp_sensitivity = temp_tp / (temp_tp + temp_fn)
                    temp_f1 = 2 * temp_precision * temp_sensitivity / (temp_precision + temp_sensitivity)
                
                if temp_tn != 0:
                    temp_specificity = temp_tn / (temp_tn + temp_fp)
                    temp_npv = temp_tn / (temp_tn + temp_fn)
                
                fpr = 1 - temp_specificity
                balanced_accuracy = (temp_sensitivity + temp_specificity) / 2

                if cutoff == 0.5:
                    test_perm.append([algo, 'bAccuracy', balanced_accuracy, 'rep' + str(i)])
                    test_perm.append([algo, 'Precision', temp_precision, 'rep' + str(i)])
                    test_perm.append([algo, 'Sensitivity', temp_sensitivity, 'rep' + str(i)])
                    test_perm.append([algo, 'Specificity', temp_specificity, 'rep' + str(i)])
                    test_perm.append([algo, 'NPV', temp_npv, 'rep' + str(i)])

                metric_perm.append([cutoff, algo, 'bAccuracy', balanced_accuracy, 'rep' + str(i)])
                metric_perm.append([cutoff, algo, 'Precision', temp_precision, 'rep' + str(i)])
                metric_perm.append([cutoff, algo, 'Sensitivity', temp_sensitivity, 'rep' + str(i)])
                metric_perm.append([cutoff, algo, 'Specificity', temp_specificity, 'rep' + str(i)])
                metric_perm.append([cutoff, algo, 'NPV', temp_npv, 'rep' + str(i)])

    print('\t\tGenerating figure - performance_on_test_set', file=log_fh)
    print('\t\tGenerating figure - performance_on_test_set', file=sys.stderr)
    test_perm_df = pd.DataFrame(test_perm, columns=['Algorithm', 'Metric', 'Value', 'Rep'])

    fig, ax = plt.subplots(figsize=(12,8))
    g = sns.boxplot(data=test_perm_df, x='Metric', y='Value', hue='Algorithm', linewidth=0.5, flierprops=FLIERPROPS, **PROPS)
    plt.xlabel('Metric', fontsize = 10)
    plt.ylabel('Performance', fontsize = 10)
    plt.ylim((0, 1))
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=8)
    plt.title('Performance on 20 random test sets', fontsize = 10)
    plt.legend(loc='best', fontsize = 10, title='Algorithm', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
    plt.savefig(output_dir + '/performance_on_test_set.pdf', dpi=600, format='pdf')
    plt.close(fig)

    unique_algorithms = test_perm_df.Algorithm.unique()
    unique_metrics = test_perm_df.Metric.unique()

    for unique_algorithm in unique_algorithms:

        level_one_df = test_perm_df.loc[test_perm_df['Algorithm'] == unique_algorithm]

        for unique_metric in unique_metrics:

            level_two_df = level_one_df.loc[level_one_df['Metric'] == unique_metric]
            print('\t\t\tAlgorithm %s - Metric %s - Median %f' % (unique_algorithm, unique_metric, level_two_df['Value'].median()), file=log_fh)
            print('\t\t\tAlgorithm %s - Metric %s - Median %f' % (unique_algorithm, unique_metric, level_two_df['Value'].median()), file=sys.stderr)

    print('\t\tGenerating figure - feature_importance_on_test_set', file=log_fh)
    print('\t\tGenerating figure - feature_importance_on_test_set', file=sys.stderr)
    feat_imp_perm_df = pd.DataFrame(feat_imp_perm, columns=['Algorithm', 'Antigen', 'Mean_Imp', 'Rep'])

    fig, ax = plt.subplots(figsize=(12,8))
    g = sns.boxplot(data=feat_imp_perm_df, x='Antigen', y='Mean_Imp', hue='Algorithm', linewidth=0.5, flierprops=FLIERPROPS, **PROPS)
    plt.xlabel('Antigen', fontsize = 10)
    plt.ylabel('Mean Importance Score', fontsize = 10)
    plt.xticks(rotation=20, fontsize=8, ha='right')
    plt.yticks(fontsize=8)
    plt.title('Feature importance after 50 permutations on 20 random test sets', fontsize = 10)
    plt.legend(loc='upper right', fontsize = 10, title='Algorithm', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
    plt.savefig(output_dir + '/feature_importance_on_test_set.pdf', dpi=600, format='pdf')
    plt.close(fig)

    print('\t\tGenerating figure - metrics_on_test_set', file=log_fh)
    print('\t\tGenerating figure - metrics_on_test_set', file=sys.stderr)
    metric_perm_df = pd.DataFrame(metric_perm, columns=['Cutoff', 'Algorithm', 'Metric', 'Value', 'Rep'])

    for algo_name in algo_list:

        tmp_df = metric_perm_df.loc[metric_perm_df['Algorithm'] == algo_name]

        fig, ax = plt.subplots(figsize=(12,8))
        g = sns.lineplot(data=tmp_df, x='Cutoff', y='Value', hue='Metric', linewidth=0.5)
        plt.xlabel('Score Cutoff', fontsize = 10)
        plt.ylabel('Score', fontsize = 10)
        plt.ylim((0, 1))
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=8)
        plt.title('Performance curves on 20 random test sets', fontsize = 10)
        plt.legend(loc='best', fontsize = 10, title='Metric', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
        plt.savefig(output_dir + '/metrics_on_test_set.' + algo_name + '.pdf', dpi=600, format='pdf')
        plt.close(fig)

    fig, ax = plt.subplots(figsize=(12,8))
    g = sns.lineplot(data=metric_perm_df, x='Cutoff', y='Value', hue='Metric', linewidth=0.5)
    plt.xlabel('Score Cutoff', fontsize = 10)
    plt.ylabel('Score', fontsize = 10)
    plt.ylim((0, 1))
    plt.xticks(fontsize=8)
    plt.yticks(fontsize=8)
    plt.title('Performance curves (all algorithms) on 20 random test sets', fontsize = 10)
    plt.legend(loc='best', fontsize = 10, title='Metric', title_fontsize = 10, fancybox=True, facecolor='lightgrey')
    plt.savefig(output_dir + '/metrics_on_test_set.pdf', dpi=600, format='pdf')
    plt.close(fig)

    print('\t\tCalculating list of most important features', file=log_fh)
    print('\t\tCalculating list of most important features', file=sys.stderr)
    new_list_of_orfs = dict()
    for algo in feat_imp_perm_dict:
    
        for orf_name in feat_imp_perm_dict[algo]:

            orf_mean = statistics.median(feat_imp_perm_dict[algo][orf_name])

            if orf_mean >= 0.01:
                
                if algo not in new_list_of_orfs:
                    new_list_of_orfs[algo] = list()

                new_list_of_orfs[algo].append(orf_name)
    
    return new_list_of_orfs


if __name__ == '__main__':
    main()

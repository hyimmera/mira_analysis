# Install packages if it's necessary
#install.packages("dplyr","plyr","stringr","data.table","ggplot2","ggrepel","ggthemes","generics","stringr","plyr","gridExtra","grid","devtools","viridis","hrbrthemes","GLDEX","TSDT","rstudioapi")

#Load needed libraries
library(cluster)
library(viridisLite)
library(spacefillr)
library(plyr)
library(dplyr)
library(stringr)
library(data.table)
library(ggplot2)
library(ggrepel)
library(ggthemes)
library(generics)
library(stringr)
library(plyr)
library(readxl)
library(gridExtra)
library(grid)
# library(devtools)
library(viridis)
# library(hrbrthemes)
library(GLDEX)
library(TSDT)
library(rstudioapi)

cur_dir = dirname(getSourceEditorContext()$path)
setwd(cur_dir)

#Make a new dir where all plots will be located
dir.create("plots_Cross_Reactivity_CD8")

#Create a folder in which the filtered files of McPAS, TCR3d and VDJdb will be located
dir.create("filtering_V_genes_Cross_Reactivity_CD8")

#Create a folder in which the supplementary files will be located
dir.create("supplementary_Cross_Reactivity_CD8")


#Change directory to public databases' folder 
setwd("../5.Repositories/Public_TCR_databases")

#################################### McPAS processing #######################################################
#####################################################################################################################

#Load data from McPAS TCR
McPAS = read.csv("McPAS-TCR.csv", header = TRUE, sep = ",")
#Filtering keeping only data for human host 
Data_human = McPAS[which(McPAS$Species == "Human"),]
Data_human = Data_human[which(Data_human$CDR3.beta.aa != "NA"),]
#data i'm gonna use
data_McPAS_human = Data_human

##############FIX Fuzzy effect problem for McPAS
data_McPAS_human[which(data_McPAS_human$Pathology == "Yellow fever virus"),5] = "YFV"

data_McPAS_human[which(data_McPAS_human$Pathology == "Psoriatic Arthritis"),5] = "Psoriatic arthritis"

data_McPAS_human[which(data_McPAS_human$Pathology == "M. tuberculosis"),5] = "M.tuberculosis"
data_McPAS_human[which(data_McPAS_human$Pathology == "M.Tuberculosis"),5] = "M.tuberculosis"

data_McPAS_human[which(data_McPAS_human$Pathology == "Influenza A"),5] = "Influenza"
data_McPAS_human[which(data_McPAS_human$Pathology == "influenza A"),5] = "Influenza"
data_McPAS_human[which(data_McPAS_human$Pathology == "InfluenzaA"),5] = "Influenza"
data_McPAS_human[which(data_McPAS_human$Pathology == "influenzaA"),5] = "Influenza"
data_McPAS_human[which(data_McPAS_human$Pathology == "influenza"),5] = "Influenza"

data_McPAS_human[which(data_McPAS_human$Pathology == "Human immunodeficiency virus (HIV)"),5] = "HIV"
data_McPAS_human[which(data_McPAS_human$Pathology == "HIV-1"),5] = "HIV"
data_McPAS_human[which(data_McPAS_human$Pathology == "HIV1"),5] = "HIV"

data_McPAS_human[which(data_McPAS_human$Pathology == "Hepatitis C virus (HCV)"),5] = "HCV"

data_McPAS_human[which(data_McPAS_human$Pathology == "Cytomegalovirus (CMV)"),5] = "CMV"
data_McPAS_human[which(data_McPAS_human$Pathology == "Cytomegalovirus"),5] = "CMV"
data_McPAS_human[which(data_McPAS_human$Pathology == "HCMV"),5] = "CMV"

data_McPAS_human[which(data_McPAS_human$Pathology == "COVID-19"),5] = "SARS-CoV-2"

data_McPAS_human[which(data_McPAS_human$Pathology == "Epstein Barr virus (EBV)"),5] = "EBV"
data_McPAS_human[which(data_McPAS_human$Pathology == "Epstein Barr virus"),5] = "EBV"

####################FILTERING STUFF

#Filtering for C/F,W 
data_McPAS_human = data_McPAS_human[which((str_sub(data_McPAS_human$CDR3.beta.aa, 1, 1) == "C") & (str_sub(data_McPAS_human$CDR3.beta.aa, -1, -1) %in% c("F","W"))),]
data_McPAS_human = data_McPAS_human[which(data_McPAS_human$Remarks %nin% c("No final F", "No first Cysteine", "No first Cysteine, No final F")),]
#Filtering for unproductive rearrangements
data_McPAS_human = data_McPAS_human[which(!grepl("unproductive", data_McPAS_human$CDR3.beta.aa)),]
#Filtering for special characters
data_McPAS_human = data_McPAS_human[which(!grepl("\\X", data_McPAS_human$CDR3.beta.aa) & !grepl("\\#", data_McPAS_human$CDR3.beta.aa) & !grepl("\\*", data_McPAS_human$CDR3.beta.aa) & !grepl("\\.", data_McPAS_human$CDR3.beta.aa)),]
#Filtering in order to remove observetions related to method identification 3
data_McPAS_human = data_McPAS_human[which(data_McPAS_human$Antigen.identification.method %nin% c(3,3.0)),]
#Filter in order to remove observations with Remarks = verify VJ
data_McPAS_human = data_McPAS_human[!grepl("verify VJ", data_McPAS_human$Remarks),]


##################################################################################################################################
#################################### END of McPAS proccesing #####################################################################



#############################################Processing TCR3d database#########################################
##############################################################################################################

#Loading TCR3d
TCR3d = read.csv("TCR3d.csv", header = TRUE, sep = ",")

###################Fix fuzzy effect for TCR3d
TCR3d[which(TCR3d$Virus.BR.Type == "Yellow fever virus"),1] = "YFV"

TCR3d[which(TCR3d$Virus.BR.Type == "Psoriatic Arthritis"),1] = "Psoriatic arthritis"

TCR3d[which(TCR3d$Virus.BR.Type == "M. tuberculosis"),1] = "M.tuberculosis"
TCR3d[which(TCR3d$Virus.BR.Type == "M.Tuberculosis"),1] = "M.tuberculosis"

TCR3d[which(TCR3d$Virus.BR.Type == "Influenza A"),1] = "Influenza"
TCR3d[which(TCR3d$Virus.BR.Type == "influenza A"),1] = "Influenza"
TCR3d[which(TCR3d$Virus.BR.Type == "InfluenzaA"),1] = "Influenza"
TCR3d[which(TCR3d$Virus.BR.Type == "influenzaA"),1] = "Influenza"
TCR3d[which(TCR3d$Virus.BR.Type == "influenza"),1] = "Influenza"

TCR3d[which(TCR3d$Virus.BR.Type == "Human immunodeficiency virus (HIV)"),1] = "HIV"
TCR3d[which(TCR3d$Virus.BR.Type == "HIV-1"),1] = "HIV"
TCR3d[which(TCR3d$Virus.BR.Type == "HIV1"),1] = "HIV"

TCR3d[which(TCR3d$Virus.BR.Type == "Hepatitis C virus (HCV)"),1] = "HCV"

TCR3d[which(TCR3d$Virus.BR.Type == "Cytomegalovirus (CMV)"),1] = "CMV"
TCR3d[which(TCR3d$Virus.BR.Type == "Cytomegalovirus"),1] = "CMV"
TCR3d[which(TCR3d$Virus.BR.Type == "HCMV"),1] = "CMV"

TCR3d[which(TCR3d$Virus.BR.Type == "COVID-19"),1] = "SARS-CoV-2"

TCR3d[which(TCR3d$Virus.BR.Type == "Epstein Barr virus (EBV)"),1] = "EBV"
TCR3d[which(TCR3d$Virus.BR.Type == "Epstein Barr virus"),1] = "EBV"

#################Filtering Stuff
#Filtering for C/F,W 
TCR3d = TCR3d[which((str_sub(TCR3d$CDR3.beta., 1, 1) == "C") & (str_sub(TCR3d$CDR3.beta., -1, -1) %in% c("F","W"))),]
#Filtering for unproductive rearrangements
TCR3d = TCR3d[which(!grepl("unproductive", TCR3d$CDR3.beta.)),]
#Filtering for special characters
TCR3d = TCR3d[which(!grepl("\\X", TCR3d$CDR3.beta.) & !grepl("\\#", TCR3d$CDR3.beta.) & !grepl("\\*", TCR3d$CDR3.beta.) & !grepl("\\.", TCR3d$CDR3.beta.)),]
#changing collumn names
colnames(TCR3d)[7] = "CDR3.beta.aa" 

########################### END of TCR3d processing ########################################################
##########################################################################################################



################################################# Processing VDJdb database ######################################
###################################################################################################################

#Load VDJdb 
VDJdb = read.csv("SearchTable-2022-03-22 16_30_04.804.tsv", header = TRUE, sep = "\t")


#######FIX FUZZY effect problem

VDJdb[which(VDJdb$Epitope.species == "Yellow fever virus"),12] = "YFV"

VDJdb[which(VDJdb$Epitope.species == "Psoriatic Arthritis"),12] = "Psoriatic arthritis"

VDJdb[which(VDJdb$Epitope.species == "M. tuberculosis"),12] = "M.tuberculosis"
VDJdb[which(VDJdb$Epitope.species == "M.Tuberculosis"),12] = "M.tuberculosis"

VDJdb[which(VDJdb$Epitope.species == "Influenza A"),12] = "Influenza"
VDJdb[which(VDJdb$Epitope.species == "influenza A"),12] = "Influenza"
VDJdb[which(VDJdb$Epitope.species == "InfluenzaA"),12] = "Influenza"
VDJdb[which(VDJdb$Epitope.species == "influenzaA"),12] = "Influenza"
VDJdb[which(VDJdb$Epitope.species == "influenza"),12] = "Influenza"

VDJdb[which(VDJdb$Epitope.species == "Human immunodeficiency virus (HIV)"),12] = "HIV"
VDJdb[which(VDJdb$Epitope.species == "HIV-1"),12] = "HIV"
VDJdb[which(VDJdb$Epitope.species == "HIV1"),12] = "HIV"

VDJdb[which(VDJdb$Epitope.species == "Hepatitis C virus (HCV)"),12] = "HCV"

VDJdb[which(VDJdb$Epitope.species == "Cytomegalovirus (CMV)"),12] = "CMV"
VDJdb[which(VDJdb$Epitope.species == "Cytomegalovirus"),12] = "CMV"
VDJdb[which(VDJdb$Epitope.species == "HCMV"),12] = "CMV"

VDJdb[which(VDJdb$Epitope.species == "COVID-129"),12] = "SARS-CoV-2"

VDJdb[which(VDJdb$Epitope.species == "Epstein Barr virus (EBV)"),12] = "EBV"
VDJdb[which(VDJdb$Epitope.species == "Epstein Barr virus"),12] = "EBV"

#############Filtering stuff

#Filtering for score ...i keep only values>0...only 
VDJdb = VDJdb[which(VDJdb$Score != 0),]
#Filtering to keep only information for Human
VDJdb = VDJdb[which(VDJdb$Species == "HomoSapiens"),]
#Filtering to keep only the B chain of TCR info
VDJdb = VDJdb[which(VDJdb$Gene == "TRB"),]
#Filtering for C/F,W 
VDJdb = VDJdb[which((str_sub(VDJdb$CDR3, 1, 1) == "C") & (str_sub(VDJdb$CDR3, -1, -1) %in% c("F","W"))),]
#Filtering for unproductive rearrangements
VDJdb = VDJdb[which(!grepl("unproductive", VDJdb$CDR3)),]
#Filtering for special characters
VDJdb = VDJdb[which(!grepl("\\X", VDJdb$CDR3) & !grepl("\\#", VDJdb$CDR3) & !grepl("\\*", VDJdb$CDR3) & !grepl("\\.", VDJdb$CDR3)),]
colnames(VDJdb)[3] = "CDR3.beta.aa" 


######################################################### END of VDJdb processing ################
##################################################################################################



#################################################################################################
#Filter to remove pseudogenes/ORFs in databases of pathogens and diseases (McPAS, TCR3d, VDJdb) 


#change directory to the folder for filtering orfs and pseudogenes
setwd("../../2.Cross_Reactivity_Analysis_all_TCRs/filtering_V_genes_Cross_Reactivity_CD8")

#Function to keep only functional V genes and remove ORFs and pseudogenes / Function variables : X = working dataframe , Y = working column , Z = name of the output file
functional_only_DB = function(X, Y, Z) {
  
  X = X[which(
    #V1
    !grepl("TRBV01", X[,Y]) &
      !grepl("TRBV1-", X[,Y]) &
      X[,Y] != "TRBV1"  &
      # FILTER V3 ?ʸ?? KAI V3 -2 (TO KEEP ONLY V3-1)
      !grepl("TRBV03-02", X[,Y]) &  
      !grepl("TRBV3-02", X[,Y]) &  
      !grepl("TRBV3-2", X[,Y]) &
      !grepl("TRBV03-2", X[,Y]) &
      X[,Y] != "TRBV03" &
      X[,Y] != "TRBV3" &
      # FILTER V12 ????? KAI 12-1 12-2
      X[,Y] != "TRBV12" &
      !grepl("TRBV12-1", X[,Y]) &
      !grepl("TRBV12-01", X[,Y]) &
      !grepl("TRBV12-2", X[,Y]) &
      !grepl("TRBV12-02", X[,Y]) &
      #FILTER EVERYTHING ABOUT TRBV21
      !grepl("TRBV21-01", X[,Y]) &  
      !grepl("TRBV21-1", X[,Y]) &
      !grepl("TRBV21", X[,Y]) &
      #FILTER EVERYTHING ABOUT TRBV26
      !grepl("TRBV26", X[,Y]) &  
      #FILTER ????? TRBV5 AND TRBV5-3
      X[,Y] != "TRBV5" &
      X[,Y] != "TRBV05" &
      !grepl("TRBV5-3", X[,Y]) &
      !grepl("TRBV05-03", X[,Y]) &
      !grepl("TRBV05-3", X[,Y]) &
      !grepl("TRBV5-03", X[,Y]) &  
      !grepl("TRBV5-7", X[,Y]) &
      !grepl("TRBV05-07", X[,Y]) &
      !grepl("TRBV05-7", X[,Y]) &
      !grepl("TRBV5-07", X[,Y]) &  
      #FILTER ????? TRBV6 ??? TRBV6-7
      X[,Y] != "TRBV6" &
      X[,Y] != "TRBV06" &  
      !grepl("TRBV6-7", X[,Y]) &
      !grepl("TRBV06-07", X[,Y]) &
      !grepl("TRBV06-7", X[,Y]) &
      !grepl("TRBV6-07", X[,Y]) &    
      #FILTER ????? TRBV7 ??? TRBV7-1
      X[,Y] != "TRBV7" &
      X[,Y] != "TRBV07" &  
      !grepl("TRBV7-1", X[,Y]) &
      !grepl("TRBV07-01", X[,Y]) &
      !grepl("TRBV07-1", X[,Y]) &
      !grepl("TRBV7-01", X[,Y]) &   
      #FILTER TRBV17
      !grepl("TRBV17", X[,Y]) &
      !grepl("TRBV23", X[,Y])),]
  
  X = X[!is.na(X[,Y]),]  
  write.csv(X, Z, row.names = FALSE)
  
}

#Run function in order to Filter the databases 

functional_only_DB(X = data_McPAS_human, Y = 22, Z = "data_McPAS_FILTERED.csv")

functional_only_DB(X = TCR3d, Y = 6, Z = "TCR3d_FILTERED.csv")

functional_only_DB(X = VDJdb, Y = 4, Z = "VDJdb_FILTERED.csv")

#NOW you can load the filtered (from pseudogenes and ORFs databases again)

data_McPAS_human = read.csv("data_McPAS_FILTERED.csv", header = TRUE)

TCR3d = read.csv("TCR3d_FILTERED.csv", header = TRUE)

VDJdb = read.csv("VDJdb_FILTERED.csv", header = TRUE)

###########################################################################################
####### End with pseudogene/ORFs filtering #########################################################


####################Creating one dataframe with all CDR3s from the databases VDJdb, TCR3d, McPAS ###################
####  ONLY 2 columns, CDR3 AA seq / PATHOLOGY

#making a data frame with cdr3 and the pathology for McPAS
McPAS_cdr3 = select(data_McPAS_human, CDR3.beta.aa, Pathology, T.Cell.Type)
#find rows with T cell type ... CD4,CD8
rows_CD4_CD8_McPAS_cdr3 = which(McPAS_cdr3$T.Cell.Type == "CD4,CD8")
#make a df to fill it with broken rows
DF_separated_CD4_CD8 = data.frame(CDR3.beta.aa = "", Pathology = "", T.Cell.Type = "")
DF_separated_CD4_CD8 = DF_separated_CD4_CD8[-1,]
#Entries with double recognition (CD4 and CD8) will brake in two rows, one with CD4 and one with CD8
for (i in rows_CD4_CD8_McPAS_cdr3) {
  
  #define current row
  current_row = McPAS_cdr3[i,]
  #make 2 rows 
  rowCD4 = current_row
  rowCD4$T.Cell.Type = "CD4"
  rowCD8 = current_row
  rowCD8$T.Cell.Type = "CD8"
  #add the 2 new rows in Df
  DF_separated_CD4_CD8 = rbind(DF_separated_CD4_CD8, rowCD4, rowCD8)
  
  print(i)
}
#now remove all rows with T cell type CD4,CD8
McPAS_cdr3 = McPAS_cdr3[-rows_CD4_CD8_McPAS_cdr3,]
#now add DF_separated_CD4_CD8 to the McPAS_cdr3 table
McPAS_cdr3 = rbind(McPAS_cdr3, DF_separated_CD4_CD8)



#making a data frame with cdr3 and the virus that it is correlated for TCR3d
TCR3d_cdr3 = select(TCR3d, CDR3.beta.aa, Virus.BR.Type, Reference)
colnames(TCR3d_cdr3)[2] = "Pathology"
colnames(TCR3d_cdr3)[3] = "T.Cell.Type"

TCR3d_cdr3[which(TCR3d_cdr3$T.Cell.Type == "27111229"),3] = "CD4"
TCR3d_cdr3[which(TCR3d_cdr3$T.Cell.Type == "28423320"),3] = "CD8"
TCR3d_cdr3[which(TCR3d_cdr3$T.Cell.Type == "30575715"),3] = "CD8"
TCR3d_cdr3[which(TCR3d_cdr3$T.Cell.Type == "24121927"),3] = "CD8"



#making data frame with cdr3 and the associated epitope species for VDJdb 
VDJdb_cdr3 = select(VDJdb, CDR3.beta.aa, Epitope.species, MHC.class)
colnames(VDJdb_cdr3)[2] = "Pathology"
colnames(VDJdb_cdr3)[3] = "T.Cell.Type"
VDJdb_cdr3[which(VDJdb_cdr3$T.Cell.Type == "MHCI"),3] = "CD8"
VDJdb_cdr3[which(VDJdb_cdr3$T.Cell.Type == "MHCII"),3] = "CD4"


#a dataframe with info about cdr3s and pathology from all 3 databases (McPAS, TCR3d, VDJdb)
DB_df = unique(rbind(McPAS_cdr3, TCR3d_cdr3, VDJdb_cdr3))

#filtering to remove SARS-CoV-2 info from DB_df
DB_df = DB_df[which(DB_df$Pathology != "SARS-CoV-2"),]

#filtering to remove synthetic, McPyV, merkel cell carcinoma, nickelcontact dermatitis, triticum aestivum from DB_df
DB_df = DB_df[which(DB_df$Pathology %nin%  c("Tumor associated antigen (TAA)", "TriticumAestivum", "synthetic", 
                                             "Nickel contact dermatitis", "Merkel cell carcinoma", "MCPyV")),]

#sum all DEN virus in one for the DB_df
DB_df[which(DB_df$Pathology == "DENV1"),2] = "DENV"
DB_df[which(DB_df$Pathology == "DENV3/4"),2] = "DENV"
DB_df[which(DB_df$Pathology == "DENV2"),2] = "DENV"

#remove HomoSapiens from the pathology column
DB_df = DB_df[which(DB_df$Pathology != "HomoSapiens"),]

#Exclude duplicates
DB_df = unique(DB_df)

#change title in DB_df for cdr3
colnames(DB_df)[1] = "CDR3"



#keep only information in DB_df associated with CDR3 sequences derived from CD8+ T cells
DB_df = DB_df[which(DB_df$T.Cell.Type == "CD8"),]


###########################################################################################################
######## The dataframe DB_df with 2 collumns (CDR3 AA seq / associated Pathology) is ready#################



# Change directory to acces MIRA dataset
setwd("../../5.Repositories/MIRA_dataset")



#########################################################################################################
###############Processing MIRA dataset (SARS-CoV-2 specific TCRs) #######################################

#Loading file for subject metadata
MIRA_subject_metadata = read.csv(file = "subject-metadata.csv", header = T)


#Load file for peptide 1 dataset
####################################################################################

MIRA_peptide_detail_ci.csv = read.csv(file = "peptide-detail-ci.csv", header = T)
#make a new collumn in peptide 1 details for CDR3 AA seq
CDR3 = str_split(MIRA_peptide_detail_ci.csv$TCR.BioIdentity, "\\+", simplify = TRUE)[,1]
MIRA_peptide_detail_ci.csv = cbind(MIRA_peptide_detail_ci.csv, CDR3)

#FILTERING peptide 1 dataframe

#Filtering for C/F,W 
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which((str_sub(MIRA_peptide_detail_ci.csv$CDR3, 1, 1) == "C") & (str_sub(MIRA_peptide_detail_ci.csv$CDR3, -1, -1) %in% c("F","W"))),]
#Filtering for unproductive rearrangements
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(!grepl("unproductive", MIRA_peptide_detail_ci.csv$CDR3)),]
#Filtering for special characters
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(!grepl("\\X", MIRA_peptide_detail_ci.csv$CDR3) & !grepl("\\#", MIRA_peptide_detail_ci.csv$CDR3) & !grepl("\\*", MIRA_peptide_detail_ci.csv$CDR3) & !grepl("\\.", MIRA_peptide_detail_ci.csv$CDR3)),]

###################################################################################



#Rename colnames for Antigen target (ORF) in peptide 1 
colnames(MIRA_peptide_detail_ci.csv)[4]= "ORF"



###Filtering MIRA dataset to use only clonotypes detected in Convalescent and Healthy cohorts

#EXPERIMENTS ASSOCIATED WITH CONVALESCENT AND HEALTHY COHORT
experiments_cov_helth = MIRA_subject_metadata[which(MIRA_subject_metadata$Cohort %in% c("Healthy (No known exposure)", "COVID-19-Convalescent")),1]
#SUBSETING minigene pep1 for experiments associated with Healthy and Convalescent
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$Experiment %in% experiments_cov_helth),]
#Subseting subject metadata to keep only info about healthy and convalescent
MIRA_subject_metadata = MIRA_subject_metadata[which(MIRA_subject_metadata$Cohort %in% c("Healthy (No known exposure)", "COVID-19-Convalescent")),]

####Filter in order to remove sequences that appear other times with family and subfamily FS and other times only in family 

###############################################################################################################

#in peptide 1 dataset
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$TCR.BioIdentity %nin% c("CASSPGGGGNQPQHF+TCRBV06-02/06-03+TCRBJ01-05", "CASSPGGGGNQPQHF+TCRBV06-02+TCRBJ01-05",
                                                                                                                 "CASSLEGAGITDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGITDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                                 "CASSLEGAGVTDTQYF+TCRBV06-02/06-03+TCRBJ02-03", "CASSLEGAGVTDTQYF+TCRBV06-02+TCRBJ02-03",
                                                                                                                 "CASSPSNEQFF+TCRBV06-02/06-03+TCRBJ02-01", "CASSPSNEQFF+TCRBV06-02+TCRBJ02-01",
                                                                                                                 "CASSPANEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSPANEQYF+TCRBV06-02+TCRBJ02-07",
                                                                                                                 "CASSQGGYEQYF+TCRBV06-02/06-03+TCRBJ02-07", "CASSQGGYEQYF+TCRBV06-02+TCRBJ02-07")),]



###########################################################################################################################################################################################################



#Change directory to the folder associated with pseudogene and orf filtering to keep only productive V genes
setwd("../../3.Cross_Reactivity_Analysis_CD8_TCRs/filtering_V_genes_Cross_Reactivity_CD8")

###############################################################################################################
###########################Filter 3 MIRA dataframes (minigene, peptide 1, peptide 2) for pseudogenes/ORFs######

#Function to keep only functional V genes and remove ORFs and pseudogenes AND remove V genes reported as "X" / Function variables : X = working dataframe , Y = working column , Z = name of the output file
functional_only = function(X, Y, Z) {
  
  X = X[which(
    !grepl("V01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("V1-", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("03-02", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("03-2", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &    
      !grepl("3-02", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &    
      !grepl("3-2", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &    
      
      !grepl("05-03", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("05-3", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V5-3", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V5-03", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("05-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("05-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V5-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) & 
      !grepl("V5-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("06-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("06-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V6-7", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V6-07", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("07-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("07-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("V7-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V7-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("12-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("12-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("12-2", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("12-02", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("V17-", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("V17", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("21-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("21-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) & 
      
      !grepl("23-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      !grepl("23-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      
      !grepl("V26-01", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &  
      !grepl("V26-1", str_split(X[,Y], "\\+", simplify = TRUE)[,2]) &
      
      !grepl("-X", str_split(X[,Y], "\\+", simplify = TRUE)[,2])),]
  
  X = X[!is.na(X[,Y]),]
  
  
  write.csv(X, Z, row.names = FALSE)
  
}

#Run the function to remove pseudogenes/ORFs and V genes reported as "X" in MIRA datasets 

functional_only(X = MIRA_peptide_detail_ci.csv, Y = 1, Z = "MIRA_peptide_detail_ci_FILTERED.csv")


## now load filtered for pseudogenes/ORFs and "X" V genes in MIRA dataset 

#pep 1
MIRA_peptide_detail_ci.csv = read.csv("MIRA_peptide_detail_ci_FILTERED.csv", header = TRUE)



######## BREAK entries with more than one antigen targets in MIRA datasets#################################
###########################################################################################################

#Break observations of peptide1 where there are more than one orfs and make a row for each one orf

#make a dataframe containing all rows with more than one orfs 
pep1_df_with_commas = MIRA_peptide_detail_ci.csv[grepl(",", MIRA_peptide_detail_ci.csv$ORF),]
#remove these rows with more than one orfs from the original pep1 dataset
MIRA_peptide_detail_ci.csv = MIRA_peptide_detail_ci.csv[(!grepl(",", MIRA_peptide_detail_ci.csv$ORF)),]
#making a new table with the orfs from the pep1_df_with_commas BROKEN
BROKEN_df = data.frame(TCR.BioIdentity = "", TCR.Nucleotide.Sequence = "", Experiment = "", ORF = "", Amino.Acids = "", 
                       Start.Index.in.Genome = "", End.Index.in.Genome = "", CDR3 = "")
splited = str_split(pep1_df_with_commas$ORF, ",", simplify = T)
for (i in 1:nrow(pep1_df_with_commas)) {
  
  row_1 = pep1_df_with_commas[i,]
  row_1$ORF = splited[i,1]
  row_2 = pep1_df_with_commas[i,]
  row_2$ORF = splited[i,2]
  BROKEN_df = rbind(BROKEN_df, row_1, row_2)
}
#remove first collumn which is empty 
BROKEN_df = BROKEN_df[-1,]
#now add BROKEN df to the original MIRA pep1 where i have removed the rows with two orfs
MIRA_peptide_detail_ci.csv = rbind(MIRA_peptide_detail_ci.csv, BROKEN_df)

################################## End with break "comma entries" observations (more than one orfs) in peptide 1 dataset############################################################
#################################################################################################################





################### CROSS REACTIVITY ANALYSIS #########################################################################
##########################################################################################################

#take all cdr3s associated with SC2 
SC2_cdr3 = unique(as.character(str_split(MIRA_peptide_detail_ci.csv$TCR.BioIdentity, "\\+", simplify = TRUE)[,1]))

#find cdr3s what exist both in mira and the other databases (McPAS, TCR3d, VDJdb)
Common_cdr3 = SC2_cdr3[SC2_cdr3 %in% DB_df$CDR3]

#keep only information about common cdr3s in mira panels and make a dataframe from them with 2 collumns / CDR3 AA seq and the ORF-ANTIGEN
CDR3_MIRA_that_exist_in_other_dbs = unique(as.data.frame(MIRA_peptide_detail_ci.csv[which(MIRA_peptide_detail_ci.csv$CDR3 %in% Common_cdr3),][,c(8,4)]))

#rename collumn for the ORF
colnames(CDR3_MIRA_that_exist_in_other_dbs)[2] = "ORF in SC2"
#make a plot to see the targets distribution of common cdr3s in SC2
table = table(CDR3_MIRA_that_exist_in_other_dbs$`ORF in SC2`)


#keep only CDR3 AND Pathology from the DB_df data frame
DB_df_twocols = select(DB_df, CDR3, Pathology)

#MAKING A data frame with clonotypes existing in MIRA dataset and the other databases for pathogens and diseases and finding what they targeting
#INNER JOIN, CDR3 AA seqs for every compination between a MIRA ORF and a DB_df pathology condition
CLONOS_SC2_DB_join = inner_join(CDR3_MIRA_that_exist_in_other_dbs, DB_df_twocols, by = "CDR3")
CLONOS_SC2_DB_join = unique(CLONOS_SC2_DB_join)
#making a table for different targets for clonotypes in order to approach the unique cdr3s for every compination
dif_targets_of_clonotypes = as.data.frame(table(CLONOS_SC2_DB_join$`ORF in SC2`, CLONOS_SC2_DB_join$Pathology))

#remove rows with Freq = 0 (compinations that don't exist)
dif_targets_of_clonotypes = dif_targets_of_clonotypes[which(dif_targets_of_clonotypes$Freq != 0),]
#rename collumn
colnames(dif_targets_of_clonotypes)[3] = "unique_CDR3_counts"

#change directory to supplementary files for cross reactivity
setwd("../supplementary_Cross_Reactivity_CD8")
write.csv(dif_targets_of_clonotypes, "./Unique CDR3 counts common in SC2 and other pathogens and diseases for CD8 T cells.csv")


#change directory to the plots' folder
setwd("../plots_Cross_Reactivity_CD8")

#making a heatmap to show TCR counts that target sc2 targets and other targets
pdf("Heat map for unique CDR3 counts common between SC2 and other pathogens and diseases.pdf", width = 13)

ggplot(dif_targets_of_clonotypes, aes(Var1, Var2, fill = unique_CDR3_counts)) +
  geom_tile() + scale_fill_gradient(low="white", high="red") + theme_classic() + 
  theme(axis.text.x=element_text(angle=20,hjust=1)) + xlab("SARS-CoV-2 antigen") + ylab("Pathogens and diseases") + ggtitle("Cross reactive CDR3 counts")

dev.off()
getwd()


